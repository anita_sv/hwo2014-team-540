package noobbot;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import noobbot.adapters.CarPositionsDeserializer;
import noobbot.adapters.MsgWrapperDeserializer;
import noobbot.base.MessageType;
import noobbot.base.MsgWrapper;
import noobbot.msg.*;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.inject.Provider;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by anita on 4/17/14.
 */
public class MsgWrapperDeserializerTest {

    private static Gson gson;

    @BeforeClass
    public static void setupOnce() {
        Provider<Gson> gsonProvider = new Provider<Gson>() {
            @Override
            public Gson get() {
                return gson;
            }
        };

        GsonBuilder gsonBuilder = new GsonBuilder();
        MsgWrapperDeserializer msgWrapperDeserializer = new MsgWrapperDeserializer(gsonProvider);
        CarPositionsDeserializer carPositionsAdapter = new CarPositionsDeserializer(gsonProvider);

        gsonBuilder.registerTypeAdapter(MsgWrapper.class, msgWrapperDeserializer);
        gsonBuilder.registerTypeAdapter(CarPositions.class, carPositionsAdapter);

        gson = gsonBuilder.create();
    }

    @Test
    public void testReadJoin() {
        String message = "{\"msgType\": \"join\", \"data\": {\n" +
                "  \"name\": \"Schumacher\",\n" +
                "  \"key\": \"UEWJBVNHDS\"\n" +
                "}}";

        MsgWrapper msgWrapper = gson.fromJson(message, MsgWrapper.class);

        assertEquals(MessageType.JOIN, msgWrapper.getMsgType());
        Join join = (Join) msgWrapper.getData();
        assertEquals("Schumacher", join.getName());
        assertEquals("UEWJBVNHDS", join.getKey());
    }

    @Test
    public void testReadYourCar() {
        String message = "{\"msgType\": \"yourCar\", \"data\": {\n" +
                "  \"name\": \"Schumacher\",\n" +
                "  \"color\": \"red\"\n" +
                "}}";

        MsgWrapper msgWrapper = gson.fromJson(message, MsgWrapper.class);

        assertEquals(MessageType.YOUR_CAR, msgWrapper.getMsgType());
        CarIdentity yourCar = (CarIdentity) msgWrapper.getData();
        assertEquals("Schumacher", yourCar.getName());
        assertEquals("red", yourCar.getColor());
    }

    @Test
    public void testReadGameInit() {
        String message = "{\"msgType\": \"gameInit\", \"data\": {\n" +
                "  \"race\": {\n" +
                "    \"track\": {\n" +
                "      \"id\": \"indianapolis\",\n" +
                "      \"name\": \"Indianapolis\",\n" +
                "      \"pieces\": [\n" +
                "        {\n" +
                "          \"length\": 100.0\n" +
                "        },\n" +
                "        {\n" +
                "          \"length\": 100.0,\n" +
                "          \"switch\": true\n" +
                "        },\n" +
                "        {\n" +
                "          \"radius\": 200,\n" +
                "          \"angle\": 22.5\n" +
                "        }\n" +
                "      ],\n" +
                "      \"lanes\": [\n" +
                "        {\n" +
                "          \"distanceFromCenter\": -20,\n" +
                "          \"index\": 0\n" +
                "        },\n" +
                "        {\n" +
                "          \"distanceFromCenter\": 0,\n" +
                "          \"index\": 1\n" +
                "        },\n" +
                "        {\n" +
                "          \"distanceFromCenter\": 20,\n" +
                "          \"index\": 2\n" +
                "        }\n" +
                "      ],\n" +
                "      \"startingPoint\": {\n" +
                "        \"position\": {\n" +
                "          \"x\": -340.0,\n" +
                "          \"y\": -96.0\n" +
                "        },\n" +
                "        \"angle\": 90.0\n" +
                "      }\n" +
                "    },\n" +
                "    \"cars\": [\n" +
                "      {\n" +
                "        \"id\": {\n" +
                "          \"name\": \"Schumacher\",\n" +
                "          \"color\": \"red\"\n" +
                "        },\n" +
                "        \"dimensions\": {\n" +
                "          \"length\": 40.0,\n" +
                "          \"width\": 20.0,\n" +
                "          \"guideFlagPosition\": 10.0\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": {\n" +
                "          \"name\": \"Rosberg\",\n" +
                "          \"color\": \"blue\"\n" +
                "        },\n" +
                "        \"dimensions\": {\n" +
                "          \"length\": 40.0,\n" +
                "          \"width\": 20.0,\n" +
                "          \"guideFlagPosition\": 10.0\n" +
                "        }\n" +
                "      }\n" +
                "    ],\n" +
                "    \"raceSession\": {\n" +
                "      \"laps\": 3,\n" +
                "      \"maxLapTimeMs\": 30000,\n" +
                "      \"quickRace\": true\n" +
                "    }\n" +
                "  }\n" +
                "}}";

        MsgWrapper msgWrapper = gson.fromJson(message, MsgWrapper.class);

        assertEquals(MessageType.GAME_INIT, msgWrapper.getMsgType());
        GameInit gameInit = (GameInit) msgWrapper.getData();
    }

    @Test
    public void testReadGameStart() {
        String message = "{\"msgType\": \"gameStart\", \"data\": null}";

        MsgWrapper msgWrapper = gson.fromJson(message, MsgWrapper.class);

        assertEquals(MessageType.GAME_START, msgWrapper.getMsgType());
        assertNull(msgWrapper.getData());
    }

    @Test
    public void testReadCarPositions() {
        String message = "{\"msgType\": \"carPositions\", \"data\": [\n" +
                "  {\n" +
                "    \"id\": {\n" +
                "      \"name\": \"Schumacher\",\n" +
                "      \"color\": \"red\"\n" +
                "    },\n" +
                "    \"angle\": 0.0,\n" +
                "    \"piecePosition\": {\n" +
                "      \"pieceIndex\": 0,\n" +
                "      \"inPieceDistance\": 0.0,\n" +
                "      \"lane\": {\n" +
                "        \"startLaneIndex\": 0,\n" +
                "        \"endLaneIndex\": 0\n" +
                "      },\n" +
                "      \"lap\": 0\n" +
                "    }\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": {\n" +
                "      \"name\": \"Rosberg\",\n" +
                "      \"color\": \"blue\"\n" +
                "    },\n" +
                "    \"angle\": 45.0,\n" +
                "    \"piecePosition\": {\n" +
                "      \"pieceIndex\": 0,\n" +
                "      \"inPieceDistance\": 20.0,\n" +
                "      \"lane\": {\n" +
                "        \"startLaneIndex\": 1,\n" +
                "        \"endLaneIndex\": 1\n" +
                "      },\n" +
                "      \"lap\": 0\n" +
                "    }\n" +
                "  }\n" +
                "], \"gameId\": \"OIUHGERJWEOI\", \"gameTick\": 0}";

        MsgWrapper msgWrapper = gson.fromJson(message, MsgWrapper.class);

        assertEquals(MessageType.CAR_POSITIONS, msgWrapper.getMsgType());

        CarPositions carPositions = (CarPositions) msgWrapper.getData();
        assertEquals(2, carPositions.getCarPositionList().size());
    }

    @Test
    public void testReadThrottle() {
        String message = "{\"msgType\": \"throttle\", \"data\": 1.0}";

        MsgWrapper msgWrapper = gson.fromJson(message, MsgWrapper.class);

        assertEquals(MessageType.THROTTLE, msgWrapper.getMsgType());

        Double throttle = (Double) msgWrapper.getData();

        assertEquals(1.0, throttle, 0.001);
    }

    @Test
    public void testUnknownMessag() {
        String message = "{\"msgType\": \"Blah\", \"data\": 1.0}";

        MsgWrapper msgWrapper = gson.fromJson(message, MsgWrapper.class);

        assertEquals(MessageType.UNKNOWN, msgWrapper.getMsgType());
        assertEquals("1.0", msgWrapper.getData().toString());

    }

}
