package noobbot.planner;

import com.google.inject.Inject;
import lombok.Data;
import noobbot.controller.PhysicsStore;
import noobbot.data.SwitchLane;
import noobbot.msg.GameInit;
import noobbot.msg.PiecePosition;
import noobbot.msg.Track;
import noobbot.msg.TrackPiece;
import noobbot.physics.GamePhysics;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by anita on 4/25/14.
 */
public class ShortestSwitchPlanner implements SwitchPlanner {

    private final Map<Integer, SwitchLane> switchLaneMap = new HashMap<>();

    private final Map<Integer, SwitchResult> switchLaneResultMap = new HashMap<>();

    private final PhysicsStore physicsStore;

    private final Track track;
    private final int totalLaps;
    private final int totalPieces;

    private final int totalLanes;

    private GamePhysics gamePhysics;

    @Inject
    public ShortestSwitchPlanner(GameInit gameInit, PhysicsStore physicsStore) {
        this.physicsStore = physicsStore;
        this.track = gameInit.getRace().getTrack();
        Integer lapCount = gameInit.getRace().getRaceSession().getActualLaps() ;
        if (lapCount == null) {
            totalLaps = 4;
        } else {
            totalLaps = lapCount;
        }

        this.totalPieces = this.track.getPieces().size();

        this.totalLanes = this.track.getLanes().size();
    }

    @Override
    public Map<Integer, SwitchLane> switchPlan(PiecePosition piecePosition,
                                               SwitchLane firedNext) {

        this.gamePhysics = physicsStore.get();
        setupSwitchMap(piecePosition.getLap(), piecePosition.getPieceIndex(),
                piecePosition.getLane().getEndIndex(), firedNext);

        return switchLaneMap;
    }

    @Data
    static class SwitchResult {
        private final double length;
        private final SwitchLane option;
    }

    int getIndex(int lap, int pieceIndex, int laneIndex) {
//        int actualLap = (lap > 1) ? 1 :lap;
        return lap * totalPieces * totalLanes +
                pieceIndex * totalLanes +
                laneIndex;
    }


    private void setupSwitchMap(int lap, int pieceIndex, int laneIndex, SwitchLane firedNext) {
        do {
            int index = getIndex(lap, pieceIndex, laneIndex);

            SwitchResult switchResult = solveSwitchLane(lap, pieceIndex, laneIndex, firedNext);

            switchLaneMap.put(index, switchResult.getOption());

//            System.out.println("SWITCH: " + lap + ", " + pieceIndex + ", " +
//                    laneIndex + "  = " + switchResult.getOption() + " -- " + switchResult.getLength());

            switch(switchResult.getOption()) {
                case LEFT:
                    laneIndex = laneIndex - 1;
                    break;
                case RIGHT:
                    laneIndex =  laneIndex + 1;
                    break;
            }

            if (pieceIndex + 1 < totalPieces) {
                pieceIndex = pieceIndex + 1;
            } else {
                lap = lap + 1;
                pieceIndex = 0;
            }
        } while (lap != totalLaps);

    }

    private SwitchResult solveSwitchLane(int lap, int pieceIndex, int laneIndex,
                                         SwitchLane firedNext) {

        int index = getIndex(lap, pieceIndex, laneIndex);
        if (switchLaneResultMap.containsKey(index)) {
            return switchLaneResultMap.get(index);
        }

        TrackPiece trackPiece = this.track.getPieces().get(pieceIndex);

        double localBest = Double.MAX_VALUE;

        SwitchLane option = null;

        int nextPieceIndex;
        int nextLap;
        if (pieceIndex + 1 == totalPieces) {
            nextLap = lap + 1;
            nextPieceIndex = 0;
        } else {
            nextLap = lap;
            nextPieceIndex = pieceIndex + 1;
        }
        if (nextLap == totalLaps) {
            SwitchResult switchResult = new SwitchResult(0, SwitchLane.STAY);
            switchLaneResultMap.put(index, switchResult);
            return switchResult;
        }

        double stayRoute = solveSwitchLane(nextLap, nextPieceIndex, laneIndex, firedNext).getLength();
        if (stayRoute < localBest) {
            localBest = stayRoute;
            option = SwitchLane.STAY;
        }

        if (trackPiece.isSwitchLane()) {
            if (firedNext == null) {
                if (laneIndex > 0) {
                    double leftRoute = solveSwitchLane(nextLap, nextPieceIndex, laneIndex - 1, firedNext).getLength();
                    if (leftRoute < localBest) {
                        localBest = leftRoute;
                        option = SwitchLane.LEFT;
                    }
                }
                if (laneIndex + 1 < totalLanes) {
                    double rightRoute = solveSwitchLane(nextLap, nextPieceIndex, laneIndex + 1, firedNext).getLength();
                    if (rightRoute < localBest) {
                        localBest = rightRoute;
                        option = SwitchLane.RIGHT;
                    }
                }
            } else {
                switch (firedNext) {
                    case LEFT:
                        if (laneIndex > 0) {
                            localBest = solveSwitchLane(nextLap, nextPieceIndex, laneIndex - 1,
                                    null).getLength();
                            option = SwitchLane.LEFT;
                        }
                        break;
                    case RIGHT:
                        if (laneIndex + 1 < totalLanes) {
                            localBest = solveSwitchLane(nextLap, nextPieceIndex, laneIndex + 1,
                                    null).getLength();
                            option = SwitchLane.RIGHT;
                        }
                        break;
                    case STAY:
                        break;
                }
            }
        }

        double curvature = trackPiece.getComputedCurvature(laneIndex);

        double myLength = trackPiece.getComputedLaneLength(laneIndex);

        double myScore = myLength * Math.sqrt(Math.abs(curvature));

//        SwitchResult switchResult = new SwitchResult(myLength + localBest, option);
        SwitchResult switchResult = new SwitchResult(myScore + localBest, option);
        switchLaneResultMap.put(index, switchResult);

        return switchResult;

    }

}
