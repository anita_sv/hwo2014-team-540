package noobbot.planner;

import com.google.inject.ImplementedBy;
import noobbot.data.TurboPlan;

import java.util.List;

/**
 * Created by anita on 4/25/14.
 */
@ImplementedBy(NoobTurboPlanner.class)
public interface TurboPlanner {

    List<TurboPlan> turboPlan();
}
