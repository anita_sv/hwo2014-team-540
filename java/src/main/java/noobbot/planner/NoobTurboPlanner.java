package noobbot.planner;

import com.google.common.collect.ImmutableList;
import noobbot.data.TurboPlan;

import java.util.List;

/**
 * Created by anita on 4/25/14.
 */
public class NoobTurboPlanner implements TurboPlanner {
    @Override
    public List<TurboPlan> turboPlan() {
        return ImmutableList.of();
    }
}
