package noobbot.planner;

import com.google.inject.ImplementedBy;
import noobbot.data.PredictivePlan;
import noobbot.physics.SolvingException;
import noobbot.physics.SolvingParams;

/**
 * Created by anita on 4/25/14.
 */
@ImplementedBy(DriftPredictivePlanner.class)
public interface Predictive {

    PredictivePlan newPlan(SolvingParams solvingParams, int depth)
            throws SolvingException, InterruptedException;
}
