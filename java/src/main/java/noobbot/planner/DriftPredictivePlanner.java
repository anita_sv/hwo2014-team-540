package noobbot.planner;

import com.google.common.base.Preconditions;
import com.google.inject.Inject;
import lombok.Data;
import noobbot.controller.PhysicsStore;
import noobbot.data.FullMetric;
import noobbot.data.PositionMetric;
import noobbot.data.PredictivePlan;
import noobbot.data.TurboPlan;
import noobbot.msg.GameInit;
import noobbot.msg.PiecePosition;
import noobbot.msg.TrackLane;
import noobbot.msg.TrackPiece;
import noobbot.physics.GamePhysicsMap;
import noobbot.physics.SolvingException;
import noobbot.physics.SolvingParams;

import java.util.*;

/**
 * Created by vaidhy.gopalan on 28/04/14.
 */

public class DriftPredictivePlanner implements Predictive {

    private GameInit gameInit;
    private PhysicsStore physics;
    private TurboPlanner turboPlanner;

    @Inject
    public DriftPredictivePlanner(GameInit gameInit,
                                  PhysicsStore gamePhysics,
                                  TurboPlanner turboPlanner) {
        this.gameInit = gameInit;
        this.physics = gamePhysics;
        this.turboPlanner = turboPlanner;
    }

    public PredictivePlan newPlan(SolvingParams solvingParams, int depth)
            throws SolvingException, InterruptedException {

        Map<Integer, Integer> switchLaneMap = new HashMap<>();
        List<TurboPlan> turboPlanList = turboPlanner.turboPlan();
        Preconditions.checkNotNull(turboPlanList);

        List<TrackPiece> trackPieces = gameInit.getRace().getTrack().getPieces();
        List<TrackLane> trackLanes = gameInit.getRace().getTrack().getLanes();
        PiecePosition piecePosition = solvingParams.getPiecePosition();
        GamePhysicsMap gamePhysics = physics.get().getDefaultPhysics();
        int currentLane = piecePosition.getLane().getEndIndex();
        int lap = solvingParams.getPiecePosition().getLap();
        int currentPieceIndex = piecePosition.getPieceIndex();

        double turboFactor = 1.0;
        FullMetric entryFullMetric = solvingParams.getFullMetric();
        PiecePosition entryPiecePosition = solvingParams.getPiecePosition();

        if (entryFullMetric.getThrottleMetric().getTurboAvailable() == null) {
            // No turbo is active right now, so ..
            Preconditions.checkNotNull(turboPlanList);
        } else {
            turboFactor = entryFullMetric.getThrottleMetric().getTurboAvailable().getTurboFactor();
        }

        int localDepth = depth;
        int endPieceIndex = currentPieceIndex + depth;
        endPieceIndex %= trackPieces.size();

        // We are not in a curve any more
        while (trackPieces.get(endPieceIndex).getCurvedLength(currentLane) > 0) {
            endPieceIndex++;
            localDepth++;
            endPieceIndex %= trackPieces.size();
        }

        endPieceIndex = (endPieceIndex + 1) % trackPieces.size();

        List<PositionMetric> positions = new LinkedList<>();

        double relativeDistance = solvingParams.getFullMetric().getPositionMetric().getPosition();
        double pieceCovered = solvingParams.getPiecePosition().getInPieceDistance();

        for (int i = 0; i < localDepth; i++) {
            TrackPiece currentPiece = trackPieces.get(currentPieceIndex);

            if (currentPiece.getRadius() == 0) {
                positions.add(new PositionMetric(relativeDistance, turboFactor * gamePhysics.maxSpeed(), 0.0));
                // Assume straight pieces will even out the drift
            } else {
                double velocity = physics.get().getCurveVelocityModel().getVelocity(
                        currentPieceIndex, currentLane);
                if (velocity == 0) {
                    System.out.println("Oops.. This curve cannot be crossed");
                    // Let us go out with a bang
                    positions.add(new PositionMetric(relativeDistance, turboFactor * gamePhysics.maxSpeed(), 0.0));
                } else {
                    positions.add(new PositionMetric(relativeDistance, velocity, 0.0));
                    positions.add(new PositionMetric(relativeDistance + currentPiece.getComputedLaneLength
                            (currentLane)* 0.99 - pieceCovered, velocity, 0.0));
                }

                turboFactor = 1.0;
                for (TurboPlan turboPlan : turboPlanList) {
                    if (turboPlan.getPosition().getLap() == lap &&
                            turboPlan.getPosition().getPieceIndex() == entryPiecePosition.getPieceIndex() &&
                            turboPlan.getPosition().getInPieceDistance() < entryPiecePosition.getInPieceDistance()) {
                        turboFactor = turboPlan.getTurboFactor();
                    }
                }

            }
            relativeDistance += currentPiece.getComputedLaneLength(currentLane) - pieceCovered;
            currentPieceIndex = (currentPieceIndex + 1) % trackPieces.size();
            if (currentPieceIndex == 0) {
                lap++;
            }
            pieceCovered = 0;
        }

//        System.out.println("Before Backtrack");
//        for (PositionMetric positionMetric : positions) {
//            System.out.println(positionMetric.getPosition() + " --> " + positionMetric.getVelocity());
//        }
        backTrack(gamePhysics, positions);
//        System.out.println("After Backtrack");
//        for (PositionMetric positionMetric : positions) {
//            System.out.println(positionMetric.getPosition() + " --> " + positionMetric.getVelocity());
//        }

        return new PredictivePlan(adjustPositions(positions), switchLaneMap, turboPlanList);

    }

    private void backTrack(GamePhysicsMap gamePhysics, List<PositionMetric> positions) {
        int index = positions.size() - 1;
        PositionMetric prevPosition = positions.get(index);
        index--;

        while (index >= 0) {
            PositionMetric currPosition = positions.get(index);
            if (prevPosition.getVelocity() < currPosition.getVelocity()) {
                double distanceEstimate = prevPosition.getPosition() - currPosition.getPosition();
                double newVelocity = gamePhysics.findMaxEntryVelocity(prevPosition.getVelocity(), distanceEstimate);
                // Oops.. my stopping distance is more than one piece. Slow down more in the current piece
                if (newVelocity < currPosition.getVelocity()) {
                    currPosition = new PositionMetric(currPosition.getPosition(), newVelocity, currPosition.getAcceleration());
                    positions.remove(index);
                    positions.add(index, currPosition);
                }
            }
            prevPosition = currPosition;
            index--;
        }
    }

    private void fillInGaps(List<PositionMetric> positions) {
        int index = 0;
        PositionMetric prev = positions.get(index);
        index++;

        while (index < positions.size()) {
            PositionMetric curr = positions.get(index);
            double velocityDiff = (curr.getVelocity() - prev.getVelocity()) / 3;
            double positionDiff = (curr.getPosition() - prev.getPosition()) / 3;
            positions.add(index, new PositionMetric(prev.getPosition() + positionDiff, prev.getVelocity() + velocityDiff, 0));
            index++;
            positions.add(index, new PositionMetric(prev.getPosition() + positionDiff * 2, prev.getVelocity() + velocityDiff * 2, 0));
            index++;
            prev = curr;
            index++;
        }
    }

    private List<FullMetric> adjustPositions(List<PositionMetric> positions) {
        List<FullMetric> fullMetrics = new ArrayList<>();
        for (int i = 0; i < positions.size(); i++) {
            PositionMetric p = positions.get(i);
            fullMetrics.add(new FullMetric(new PositionMetric(p.getPosition(), p.getVelocity(), p.getAcceleration()),
                    null, null, 0.0, 0));
        }
        return fullMetrics;
    }
}
