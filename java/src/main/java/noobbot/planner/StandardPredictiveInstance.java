package noobbot.planner;

import com.google.common.base.Preconditions;
import lombok.Data;
import noobbot.data.*;
import noobbot.msg.GameInit;
import noobbot.msg.PiecePosition;
import noobbot.msg.Track;
import noobbot.msg.TrackPiece;
import noobbot.physics.GamePhysics;
import noobbot.physics.GamePhysicsMap;
import noobbot.physics.SolvingParams;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by anita on 4/26/14.
 */
public class StandardPredictiveInstance  {

    private final GamePhysics gamePhysics;
    private final Track track;
    private final int totalLaps;
    private final int totalPieces;
    private final int totalLanes;
    private final List<TurboPlan> turboPlanList;
    private final TurboPlan turboPlan;
    private final int maxDrift;

    private PredictivePlan predictivePlan;

    private final Map<Integer, Double> throttleMap = new HashMap<>();

    private final Map<Integer, Double> timeTakenMap = new HashMap<>();

    private final Map<Integer, Double> exitVelocityMap = new HashMap<>();
    private final Map<Integer, Double> exitDriftMap = new HashMap<>();
    private final Map<Integer, Double> exitDriftRateMap = new HashMap<>();

    private final Map<Integer, Double> turboInPieceDistance = new HashMap<>();

    private final Map<Integer, SwitchLane> switchLaneMap;

    private int bestTime = Integer.MAX_VALUE;

    private int nodes = 0;

    public StandardPredictiveInstance(GameInit gameInit,
                                      GamePhysics gamePhysics,
                                      List<TurboPlan> turboPlanList,
                                      int maxDrift, Map<Integer, SwitchLane> switchLaneMap) throws InterruptedException {

        this.gamePhysics = gamePhysics;
        this.switchLaneMap = switchLaneMap;
        this.track = gameInit.getRace().getTrack();
        Integer lapCount = gameInit.getRace().getRaceSession().getActualLaps();
        if (lapCount == null) {
            totalLaps = 4;
        } else {
            totalLaps = lapCount;
        }

        this.totalPieces = this.track.getPieces().size();

        this.totalLanes = this.track.getLanes().size();

        this.turboPlan = turboPlanList.isEmpty() ? null : turboPlanList.get(0);

        this.maxDrift = maxDrift;

        this.turboPlanList = turboPlanList;

    }

    private boolean solve(SolvingParams solvingParams, int depth) throws InterruptedException {
        PiecePosition piecePosition = solvingParams.getPiecePosition();


        int trials = 0;
        Result result;
        do {
            result = solveInternal(
                    piecePosition.getLap(),
                    piecePosition.getPieceIndex(),
                    piecePosition.getLane().getEndIndex(),
                    solvingParams.getFullMetric().getPositionMetric().getVelocity(),
                    0,
                    solvingParams.getFullMetric().getDriftMetric(),
                    piecePosition.getInPieceDistance(),
                    depth,
                    1.0,
                    0,
                    maxDrift + trials * 5);
            ++trials;
            if (!result.isSolved()) {
                System.out.println("Trying with maxDrift = " + (maxDrift + trials * 5));
            }
        } while (!result.isSolved() && maxDrift + trials * 5 <= 90);

        if (result.isSolved()) {
            List<FullMetric> fullMetricList = new ArrayList<>();
            Map<Integer, Integer> laneTarget = new HashMap<>();

            Preconditions.checkNotNull(turboPlanList);
            predictivePlan = new PredictivePlan(fullMetricList, laneTarget,
                    turboPlanList);

            generatePlan(
                    piecePosition.getLap(),
                    piecePosition.getPieceIndex(),
                    piecePosition.getLane().getEndIndex(),
                    solvingParams.getFullMetric().getPositionMetric().getVelocity(),
                    solvingParams.getFullMetric().getDriftMetric(),
                    solvingParams.getFullMetric().getPositionMetric().getPosition(),
                    0);
            return true;
        } else {
            return false;
        }
    }

    private void generatePlan(int lap, int pieceIndex, int laneIndex,
                              double entryVelocity, DriftMetric entryDrift,
                              double location, int startTime) {

        TrackPiece trackPiece = this.track.getPieces().get(pieceIndex);

        int index = getIndex(lap, pieceIndex, laneIndex);

        double signedCurvature = 0;
        if (trackPiece.getRadius() > 0) {
            signedCurvature = trackPiece.getComputedCurvature(laneIndex);
        }

        if (!throttleMap.containsKey(index)) {
            return;
        }

        double throttle = throttleMap.get(index);

        System.out.println("Throttle Plan: " + lap + ", " + pieceIndex + ", " + laneIndex + ", " + throttle + ", " +
                "" + exitVelocityMap.get(index) + ", " + exitDriftMap.get(index));

        int time = timeTakenMap.get(index).intValue();
        SwitchLane switchLane = switchLaneMap.get(index);

        int nextLaneIndex;
        if (switchLane != null) {
            switch (switchLane) {
                case LEFT:
                    nextLaneIndex = laneIndex - 1;
//                    System.out.println("GamePlan Switch: left");
                    break;
                case RIGHT:
                    nextLaneIndex = laneIndex + 1;
//                    System.out.println("GamePlan Switch: right");
                    break;
                case STAY:
                    nextLaneIndex = laneIndex;
//                    System.out.println("GamePlan Switch: stay");
                    break;
                default:
                    nextLaneIndex = laneIndex;
//                    System.out.println("GamePlan Switch: stay (new switch?)");
                    break;
            }
        } else {
            nextLaneIndex = laneIndex;
//            System.out.println("GamePlan Switch: stay (out of plan)");
        }

        Map<Integer, Integer> laneTarget = predictivePlan.getLaneTarget();
        int pieceId = pieceIndex + lap * totalPieces;
        laneTarget.put(pieceId, nextLaneIndex);

        // FIXME FIXME FIXME: TURBO IS NOT PLANNED RIGHT, NEEDS RECURSION MID-LENGTH.

        for (double i = 0; i < time; i = i + 1) {
            addToPlan(pieceIndex, laneIndex, entryVelocity, entryDrift, location, signedCurvature, throttle,
                    i);
        }
        addToPlan(pieceIndex, laneIndex, entryVelocity, entryDrift, location, signedCurvature, throttle,
                time);

        int nextPieceIndex;
        int nextLap;
        if (pieceIndex == totalPieces - 1) {
            nextLap = lap + 1;
            nextPieceIndex = 0;
        } else {
            nextLap = lap;
            nextPieceIndex = pieceIndex + 1;
        }
        if (nextLap == totalLaps) {
            return;
        }

        // Reduntant call, share plans!
        double exitVelocity = getGamePhysicsMap(pieceIndex, laneIndex).findVelocity(entryVelocity, throttle,
                time);
        DriftMetric exitDrift = getGamePhysicsMap(pieceIndex, laneIndex).findExitDrift(entryVelocity, throttle, time,
                entryDrift, signedCurvature, 0.5);

        generatePlan(nextLap, nextPieceIndex, nextLaneIndex, exitVelocity, exitDrift, location, startTime + time);
    }

    private int getPhysicsState(int pieceIndex, int laneIndex) {
        return pieceIndex * totalLanes + laneIndex;
    }

    private GamePhysicsMap getGamePhysicsMap(int pieceIndex, int laneIndex) {
        return gamePhysics.getGamePhysicsMap(getPhysicsState(pieceIndex, laneIndex));
    }

    private void addToPlan(int pieceIndex, int laneIndex,
                           double entryVelocity, DriftMetric entryDrift, double location,
                           double signedCurvature, double throttle, double i) {
        GamePhysicsMap gamePhysics = getGamePhysicsMap(pieceIndex, laneIndex);
        double velocity = gamePhysics.findVelocity(entryVelocity, throttle, i);
        double inPieceDistance = gamePhysics.findDistance(entryVelocity, throttle, i);
        double acceleration = gamePhysics.findAcceleration(throttle, velocity);
        DriftMetric drift = gamePhysics.findExitDrift(entryVelocity, throttle, i, entryDrift,
                signedCurvature, 0.5);
        //            System.out.println("GamePlan: " + (startTime + i - 1) + ", " + location + ", " + velocity + ", " +

        //                    "" + drift.getDriftDegrees() + ", " + throttle);

        double position = location + inPieceDistance;

        List<FullMetric> fullMetricList = predictivePlan.getFullMetricList();
        PositionMetric positionMetric = new PositionMetric(
                position,
                velocity,
                acceleration);

        // TODO(ANITA): SET TURBO FACTOR H
        ThrottleMetric throttleMetric = new ThrottleMetric(throttle,
                null, 0);

        FullMetric fullMetric = new FullMetric(positionMetric, drift, throttleMetric,
                signedCurvature, getPhysicsState(pieceIndex, laneIndex));

        fullMetricList.add(fullMetric);
    }

    int getIndex(int lap, int pieceIndex, int laneIndex) {
        return lap * totalPieces * totalLanes +
                pieceIndex * totalLanes +
                laneIndex;
    }

    public PredictivePlan getPredictivePlan(SolvingParams solvingParams,
                                            int depth) throws InterruptedException {
        solve(solvingParams, depth);
        return predictivePlan;
    }

    @Data
    private static class Result {
        private final boolean solved;
        private final int time;
    }

    private Result solveInternal(int lap, int pieceIndex, int laneIndex,
                                 double entryVelocity,
                                 int spentTime,
                                 DriftMetric entryDriftMetric,
                                 double inPieceDistance,
                                 int depth,
                                 double throttleMultiplier,
                                 long turboExpirationTime,
                                 int localDriftLimit) throws InterruptedException {
        if (Thread.interrupted()) {
            throw new InterruptedException();
        }
        ++nodes;
        if (nodes % 1000 == 0) {
            System.out.println("Nodes: " + nodes);
        }
        TrackPiece trackPiece = this.track.getPieces().get(pieceIndex);
        double laneLeft = trackPiece.getComputedLaneLength().get(laneIndex) - inPieceDistance;

        int index = getIndex(lap, pieceIndex, laneIndex);

        double signedCurvature = 0;
        if (trackPiece.getRadius() > 0) {
            signedCurvature = trackPiece.getComputedCurvature(laneIndex);
        }

        if (depth <= 0 && trackPiece.getRadius() <= 0) {
            return new Result(true, spentTime);
        }

        double nextThrottleMultiplier = 1;
        int COUNT_START = 2;

        if (turboPlan != null &&
                turboPlan.getPosition().getLap() == lap &&
                turboPlan.getPosition().getPieceIndex() == pieceIndex) {

            if (turboPlan.getPosition().getInPieceDistance() >= inPieceDistance) {
                laneLeft = turboPlan.getPosition().getInPieceDistance() - inPieceDistance;
            } else {
                COUNT_START = 10;
                nextThrottleMultiplier = turboPlan.getTurboFactor();
                turboExpirationTime = turboPlan.getTurboDurationTicks();
            }
        }

        if (spentTime > turboExpirationTime) {
            COUNT_START = 2;
            throttleMultiplier = 1.0;
        }

        double throttleNormalizer = 1.0 / COUNT_START;

        GamePhysicsMap gamePhysics = getGamePhysicsMap(pieceIndex, laneIndex);

//        double maxThrottle = gamePhysics.findMaxThrottle(signedCurvature, laneLeft,
//                entryDriftMetric.getDriftDegrees(),
//                entryDriftMetric.getDriftRateDegrees(),
//                localDriftLimit);

        throttleMultiplier = Math.min(1.0, throttleMultiplier);

        // TODO(anita): Planned throttle is not accurate!
        for (int i = COUNT_START; i >=0 ; i--) {
            double throttle = i * throttleNormalizer * throttleMultiplier;
            double time = gamePhysics.findTimeNew(entryVelocity, throttle, laneLeft);
            if (time == Double.MAX_VALUE) {
                return new Result(false, 0);
            }
            double exitVelocity = gamePhysics.findVelocity(entryVelocity, throttle, time);

            double localMaxDrift = gamePhysics.findMaxDrift(entryVelocity, throttle, time, entryDriftMetric
                    , signedCurvature, 1);

            DriftMetric exitDrift = gamePhysics.findExitDrift(entryVelocity, throttle, time, entryDriftMetric
                    , signedCurvature, 1);

            if (Math.abs(localMaxDrift) >  localDriftLimit) {
                continue;
            }

            int nextPieceIndex;
            int nextLap;
            if (pieceIndex == totalPieces - 1) {
                nextLap = lap + 1;
                nextPieceIndex = 0;
            } else {
                nextLap = lap;
                nextPieceIndex = pieceIndex + 1;
            }

            int nextTime = ((int) time) + spentTime;

            if (nextLap == totalLaps) {
                if (nextTime < bestTime) {
                    bestTime = nextTime;
                }
                if (nextTime == bestTime) {
                    throttleMap.put(index, throttle);
                    timeTakenMap.put(index, time);
                    exitVelocityMap.put(index, exitVelocity);
                    exitDriftMap.put(index, exitDrift.getDriftDegrees());
                    exitDriftRateMap.put(index, exitDrift.getDriftRateDegrees());
                }
                return new Result(true, nextTime);
            }

            SwitchLane switchLane = switchLaneMap.get(index);
            int nextLaneIndex;
            if (switchLane == null) {
                nextLaneIndex = laneIndex;
            } else {
                switch (switchLane) {
                    case LEFT:
                        nextLaneIndex = laneIndex - 1;
                        break;
                    case RIGHT:
                        nextLaneIndex = laneIndex + 1;
                        break;
                    case STAY:
                        nextLaneIndex = laneIndex;
                        break;
                    default:
                        nextLaneIndex = laneIndex;
                }
            }
            Result res = solveInternal(nextLap, nextPieceIndex, nextLaneIndex, exitVelocity, nextTime,
                    exitDrift, 0, depth - 1, nextThrottleMultiplier, turboExpirationTime, localDriftLimit);

            int bestLocalTime = Integer.MAX_VALUE;
            Result bestRes = null;

            if (res.isSolved() && res.getTime() < bestLocalTime) {
                bestRes = res;
                bestLocalTime = res.getTime();
            }

            if (bestRes != null && bestRes.isSolved()) {
                if (bestRes.getTime() < bestTime) {
                    bestTime = bestRes.getTime();
                }
                if (bestRes.getTime() == bestTime) {
                    throttleMap.put(index, throttle);
                    timeTakenMap.put(index, time);
                    exitVelocityMap.put(index, exitVelocity);
                    exitDriftMap.put(index, exitDrift.getDriftDegrees());
                    exitDriftRateMap.put(index, exitDrift.getDriftRateDegrees());
                }
//                System.out.println(pad + "LEAVE (SUCCESS) Lap: " + lap + ": PI: " + pieceIndex + " LI: " + laneIndex);
                return bestRes;
            } else {
//                System.out.println(pad + "OMG I Failed!!: " + bestRes);
            }
        }
//        System.out.println("LEAVE (US) Lap: " + lap + ": PI: " + pieceIndex + " LI: " + laneIndex);
        return new Result(false, 0);
    }

}