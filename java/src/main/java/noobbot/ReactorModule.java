package noobbot;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import noobbot.data.Cars;
import noobbot.msg.GameInit;
import noobbot.physics.GamePhysics;

/**
 * A new reactor module is made per game-init. Don't feel like factorizing the whole
 * flow.
 */
public class ReactorModule extends AbstractModule {

    private static int maxDrift = 40;

    private static final int finalMaxDrift = 50;

    private final GameInit gameInit;

    private final Cars allCarDetails;

    private final GamePhysics gamePhysics;

    public ReactorModule(GameInit gameInit, Cars allCarDetails, GamePhysics gamePhysics) {
        this.gameInit = gameInit;
        this.allCarDetails = allCarDetails;
        this.gamePhysics = gamePhysics;
    }

    public static void setMaxDrift(int maxDrift) {
        ReactorModule.maxDrift = maxDrift;
    }

    public static int getMaxDrift() {
        return maxDrift;
    }

    @Override
    protected void configure() {
        bind(GameInit.class).toInstance(gameInit);
        bind(Cars.class).toInstance(allCarDetails);
        bind(Integer.class).annotatedWith(Names.named("initialDepth")).toInstance(2);
        bind(Integer.class).annotatedWith(Names.named("maxDepth")).toInstance(30);
        bind(Integer.class).annotatedWith(Names.named("maxDrift")).toInstance(maxDrift);
        bind(Integer.class).annotatedWith(Names.named("blockingDepth")).toInstance(8);
        bind(Integer.class).annotatedWith(Names.named("urgentDepth")).toInstance(4);
        bind(Integer.class).annotatedWith(Names.named("finalMaxDrift")).toInstance(finalMaxDrift);
        bind(Double.class).annotatedWith(Names.named("defaultThrottle")).toInstance(0.3);
    }

    @Named("initialPhysics") @Provides
    public GamePhysics initialPhysics() {
        return gamePhysics;

    }
}
