package noobbot.math;

import com.google.common.collect.ImmutableList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anita on 4/19/14.
 */
public class CoarseDerivative implements Derivative {

    @Override
    public List<Double> derive(List<Double> timeSeries) {
        if (timeSeries.isEmpty()) {
            return ImmutableList.of();
        }

        List<Double> output = new ArrayList<>(timeSeries.size());
        double prev = timeSeries.get(0);
        for (Double value : timeSeries) {
            double diff = value - prev;
            prev = value;
            output.add(diff);
        }
        return output;
    }

}
