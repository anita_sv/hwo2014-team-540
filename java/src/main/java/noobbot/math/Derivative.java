package noobbot.math;

import java.util.List;

/**
 * Created by anita on 4/19/14.
 */
public interface Derivative {

    List<Double> derive(List<Double> timeSeries);
}
