package noobbot.math;

import noobbot.msg.Orientation;
import org.apache.commons.math3.linear.RealVector;

/**
 * Created by anita on 4/20/14.
 */
public class Geometry {

    private RealVector position;

    private double angle;

    public Geometry(Orientation start) {
        this.position = Polar.asVector(start.getPosition());
        this.angle = start.getAngle();
    }

    public void move(double length) {
        RealVector disp = Polar.unit(this.angle).mapMultiply(length);
        this.position = this.position.add(disp);
    }

    public void moveLateral(double length) {
        RealVector disp = Polar.unit(this.angle - 90).mapMultiply(length);
        this.position = this.position.add(disp);
    }

    public void turnLeft(double turnAngle, double turnRadius) {
        turnAngle = Math.abs(turnAngle);
        turnRadius = Math.abs(turnRadius);

        double centerAngle = angle + 90;
        RealVector centerDisp = Polar.unit(centerAngle).mapMultiply(turnRadius);
        RealVector center = this.position.add(centerDisp);

        RealVector centerRadial = centerDisp.mapMultiply(-1);

        RealVector newPositionFromCenter = Polar.rotate(turnAngle).operate(centerRadial);

        this.position = center.add(newPositionFromCenter);
        this.angle = this.angle + turnAngle;
    }

    public void turnRight(double turnAngle, double turnRadius) {
        turnAngle = Math.abs(turnAngle);
        turnRadius = Math.abs(turnRadius);

        double centerAngle = angle - 90;
        RealVector centerDisp = Polar.unit(centerAngle).mapMultiply(turnRadius);
        RealVector center = this.position.add(centerDisp);

        RealVector centerRadial = centerDisp.mapMultiply(-1);

        RealVector newPositionFromCenter = Polar.rotate(-turnAngle).operate(centerRadial);

        this.position = center.add(newPositionFromCenter);
        this.angle = this.angle - turnAngle;
    }

    public String toString() {
        return "(" + this.position.getEntry(0) + ", " +
                this.position.getEntry(1) + ") @ " +
                this.angle +" deg";
    }

    public Orientation toOrientation() {
        return new Orientation(Polar.asPosition(this.position), this.angle);
    }
}
