package noobbot.math;

import lombok.Data;

/**
 * Created by anita on 4/19/14.
 */
@Data
public class Measure {

    private final int x;

    private final int y;
}
