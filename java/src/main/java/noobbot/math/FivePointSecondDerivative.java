package noobbot.math;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anita on 4/19/14.
 */
public class FivePointSecondDerivative implements Derivative {

    private final CoarseDerivative coarseDerivative = new CoarseDerivative();
    private final FivePointDerivative fivePointDerivative = new FivePointDerivative();

    @Override
    public List<Double> derive(List<Double> timeSeries) {
        if (timeSeries.size() < 5) {
            return coarseDerivative.derive(fivePointDerivative.derive(timeSeries));
        }

        double first = timeSeries.get(0);
        double second = timeSeries.get(1);
        double third = timeSeries.get(2);
        double forth = timeSeries.get(3);

        List<Double> output = new ArrayList<>(timeSeries.size());

        output.add(first);
        output.add(((third - first) / 2.0) - first);

        for (int i = 4; i < timeSeries.size(); i++) {
            double fifth = timeSeries.get(i);
            double diff = (-first + 16 * second - 30 * third + 16 * forth - fifth) / 30;
            output.add(diff);

            first = second;
            second = third;
            third = forth;
            forth = fifth;
        }
        output.add((forth - second) / 2.0);
        output.add(forth - third);

        return output;
    }
}
