package noobbot.math;

import com.google.common.collect.ImmutableList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anita on 4/19/14.
 */
public class FivePointDerivative implements Derivative {
    @Override
    public List<Double> derive(List<Double> timeSeries) {
        if (timeSeries.isEmpty()) {
            return ImmutableList.of();
        }
        if (timeSeries.size() == 1) {
            return ImmutableList.of(timeSeries.get(0));
        }

        double first = timeSeries.get(0);
        double second = timeSeries.get(1);

        if (timeSeries.size() == 2) {
            return ImmutableList.of(first, second - first);
        }

        double third = timeSeries.get(2);
        if (timeSeries.size() == 3) {
            return ImmutableList.of(first, second - first, third - second);
        }

        double forth = timeSeries.get(3);
        if (timeSeries.size() == 4) {
            return ImmutableList.of(first, (third - first) / 2.0, (forth - second) / 2.0,
                    forth - third);
        }

        List<Double> output = new ArrayList<>();

        output.add(first);
        output.add((third - first) / 2.0);

        for (int i = 4; i < timeSeries.size(); i++) {
            double fifth = timeSeries.get(i);
            double diff = (-first + 8 * second - 8 * forth + fifth) / 12;
            output.add(diff);

            first = second;
            second = third;
            third = forth;
            forth = fifth;
        }

        output.add((forth - second) / 2.0);
        output.add(forth - third);

        return output;
    }
}
