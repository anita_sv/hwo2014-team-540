package noobbot.math;

import com.google.common.collect.ImmutableList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anita on 4/19/14.
 */
public class TwoPointDerivative implements Derivative {

    @Override
    public List<Double> derive(List<Double> timeSeries) {
        if (timeSeries.isEmpty()) {
            return ImmutableList.of();
        }
        if (timeSeries.size() == 1) {
            return ImmutableList.of(0.0);
        }
        List<Double> output = new ArrayList<>();

        double firstDiff = timeSeries.get(1) - timeSeries.get(0);
        double prev = timeSeries.get(0) - firstDiff;
        double prevBuff = timeSeries.get(0);

        for (int i = 0; i + 1 < timeSeries.size(); i++) {
            double value = timeSeries.get(i + 1);
            double diff = (value - prev) / 2.0;
            output.add(diff);
            prev = prevBuff;
            prevBuff = value;
        }
        double lastDiff = prevBuff - prev;
        output.add(lastDiff);

        return output;
    }
}
