package noobbot.math;

import lombok.Data;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.linear.SingularMatrixException;

/**
 */
public class LinearRegression {

    @Data
    public static class RegressionResult {

        private final RealVector solution;

        private final double meanSquaredError;
    }

    /**
     * Solves
     *   Ax = b
     *
     * Such that, mean square error, or || Ax - b || is minimized.
     *
     * In order to solve simple regression,
     *    y = mx + c
     *
     * Give A = [ x_1,  1 ;  x_2,  2 ]
     * And  b = [ y_1 ;  y_2 ]
     * and then x = [ m ; c ]
     *
     * @param parameters  (A)
     * @param outcome (b)
     * @return x, and MSE
     */
    public static RegressionResult solve(RealMatrix parameters, RealVector outcome)
            throws SingularMatrixException {
        try {
            RealMatrix transpose = parameters.transpose();
            RealMatrix squareMatrix = transpose.multiply(parameters);
            RealMatrix inverse = new LUDecomposition(squareMatrix).getSolver().getInverse();
            RealMatrix preProjection = inverse.multiply(transpose);
            RealVector solution = preProjection.operate(outcome);
            double mse =
                    Math.sqrt(parameters.operate(solution).subtract(outcome).getNorm()/parameters.getRowDimension());
            return new RegressionResult(solution, mse);
        } catch (SingularMatrixException e) {
            System.out.println(parameters);
            throw e;
        }
    }

}
