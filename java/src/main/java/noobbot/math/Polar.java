package noobbot.math;

import noobbot.msg.Position;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

/**
 * Created by anita on 4/20/14.
 */
public class Polar {

    public static RealMatrix rotate(double degrees) {
        double radians = degrees * Math.PI / 180;
        RealMatrix rotation = new Array2DRowRealMatrix(2, 2);
        rotation.setEntry(0, 0, Math.cos(radians));
        rotation.setEntry(1, 0, Math.sin(radians));
        rotation.setEntry(0, 1, -Math.sin(radians));
        rotation.setEntry(1, 1, Math.cos(radians));

        return rotation;
    }

    public static RealVector unit(double degrees) {
        double radians = degrees * Math.PI / 180;
        RealVector unitVector = new ArrayRealVector(2);
        unitVector.setEntry(0, Math.cos(radians));
        unitVector.setEntry(1, Math.sin(radians));
        return unitVector;
    }

    public static RealVector asVector(Position pos) {
        RealVector realVector = new ArrayRealVector(2);
        realVector.setEntry(0, pos.getX());
        realVector.setEntry(1, pos.getY());
        return realVector;
    }

    public static Position asPosition(RealVector realVector) {
        return new Position(realVector.getEntry(0),
                realVector.getEntry(1));
    }
}
