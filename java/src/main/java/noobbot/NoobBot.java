package noobbot;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Guice;
import com.google.inject.Injector;
import lombok.RequiredArgsConstructor;
import noobbot.base.MessageType;
import noobbot.controller.PhysicsStore;
import noobbot.controller.Reactor;
import noobbot.data.CarControl;
import noobbot.data.CarInfo;
import noobbot.data.Cars;
import noobbot.data.SwitchLane;
import noobbot.msg.*;
import noobbot.physics.*;
import noobbot.wire.BotListener;
import noobbot.wire.BotWriter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anita on 4/17/14.
 */
@RequiredArgsConstructor
public class NoobBot implements BotListener {

    private final BotWriter botWriter;
    private boolean inQualifying = false;
    private static Cars allCarDetails;
    private GameInit gameData = null;
    private CarIdentity myCar;
    private static CarInfo carInfo = new CarInfo();
    private boolean inCrash;
    private List<TurboAvailable> turboData = new ArrayList<>();
    private int turboBlocked = 0;

    private Injector injector;
    private Reactor reactor;

    public void init(Join join) {
        botWriter.send(MessageType.JOIN, join);
    }

    public void init(CreateRace race) {
        botWriter.send(MessageType.CREATE_RACE, race);
    }

    public void join(CreateRace race) {
        botWriter.send(MessageType.JOIN_RACE, race);
    }

    @Override
    public void handleYourCar(CarIdentity yourCar) {
        this.myCar = yourCar;
        carInfo.setMyCarIdentity(yourCar);
    }

    @Override
    public void handleGameInit(GameInit gameInit) {
        if (this.gameData == null) {
            System.out.println("Qualifying round");
            this.gameData = gameInit;
            this.inCrash = false;

            if (gameData.getRace().getRaceSession().getLaps() != null) {
                this.gameData.getRace().getRaceSession().setActualLaps(gameInit.getRace().getRaceSession().getLaps());
            }

            CurveVelocityModel2 curveVelocityModel = new CurveVelocityModel2();

            GamePhysics defaultPhysics = new GamePhysics(
                    new GamePhysicsMap(new SpeedModel(
                            new SpeedModel.ModelData(0.204, 0.0204, 0.0)),
                            new DriftModel(
                                    new DriftModel.ModelData(
                                            1.5,
                                            2.0406651427872444,
                                            0.09429410308959589,
                                            0.0))),
                    ImmutableMap.<Integer, GamePhysicsMap>of(),
                    curveVelocityModel
            );

            fillTrackDetails();
            curveVelocityModel.setTrackPieces(gameData.getRace().getTrack().getPieces());

            carInfo = new CarInfo();
            carInfo.setMyCarIdentity(myCar);
            carInfo.setGameData(gameData);

            if (reactor != null) {
                reactor.shutdown();
            }

            allCarDetails = new Cars(gameData.getRace().getTrack().getPieces(), gameData.getRace().getCars().get(0).getDimensions(), defaultPhysics);
            allCarDetails.setMyCarIdentity(myCar);



            if (this.injector != null) {
                PhysicsStore physicsStore = this.injector.getInstance(PhysicsStore.class);
                defaultPhysics = physicsStore.get();
            }

            this.injector = Guice.createInjector(new ReactorModule(gameInit, allCarDetails, defaultPhysics));

            this.reactor = injector.getInstance(Reactor.class);
            if (this.reactor == null) {
                throw new IllegalStateException();
            }

            System.out.println("Brand New Reactor setup!");
        } else {
            this.gameData.getRace().getRaceSession().setActualLaps(gameInit.getRace().getRaceSession().getLaps());


        }
    }

    private void fillTrackDetails() {
        Track track = this.gameData.getRace().getTrack();
        int index = 0;

        for (TrackPiece tp : track.getPieces()) {
            tp.setIndex(index);
            for (TrackLane lane : track.getLanes()) {

                // If it is a circular piece, calculate the actual length
                if (tp.getRadius() > 0) {

                    double actualRadius;

                    // Outer tracks are longer
                    if (tp.getAngle() > 0) {
                        actualRadius = tp.getRadius() - lane.getDistanceFromCenter();
                    } else {
                        actualRadius = tp.getRadius() + lane.getDistanceFromCenter();
                    }

                    tp.setComputedLaneRadius(lane.getIndex(), actualRadius);
                    tp.setComputedLaneLength(lane.getIndex(), 2 * Math.PI * actualRadius * Math.abs(tp.getAngle()) / 360);

                } else {
                    tp.setComputedLaneRadius(lane.getIndex(), 0);
                    tp.setComputedLaneLength(lane.getIndex(), tp.getLength());
                }
            }
            index++;

            //System.out.printf("%.3f, %d, %.3f, %.3f, %.3f %n", tp.getLength(), tp.getRadius(), tp.getAngle(),
            //        tp.getComputedLaneLength(0), tp.getComputedLaneRadius(0));
        }

        // Find the longest sequence for straight pieces for turbo

        // Find the first curved piece
        int firstCurvedPiece = -1;
        int currentTrackIndex = 0;
        while (firstCurvedPiece == -1) {
            if (track.getPieces().get(currentTrackIndex).getAngle() > 0) {
                firstCurvedPiece = currentTrackIndex;
            }
            currentTrackIndex ++;
        }


        int currentSeqBeginning = -1;
        double prevSequence = 0;
        int prevSequenceBeginning = -1;
        double currentSequence = 0;

        TrackPiece currentPiece = getPreviousPiece(firstCurvedPiece);
        currentTrackIndex = firstCurvedPiece - 1;
        if (currentTrackIndex < 0) {
            currentTrackIndex = track.getPieces().size() - 1;
        }

        if (currentPiece.getAngle() == 0) {
            currentPiece.setStraightLength(currentPiece.getLength());
            currentSeqBeginning = currentTrackIndex;
        }

        while (true) {
            TrackPiece prevPiece = getPreviousPiece(currentTrackIndex);
            // Is it straight
            if (prevPiece.getAngle() == 0) {
                // Is this a new sequence
                if (currentSeqBeginning == -1)  {
                    prevPiece.setStraightLength(prevPiece.getLength());
                    currentSeqBeginning = currentTrackIndex;
                    currentSequence = prevPiece.getLength();

                } else {
                    prevPiece.setStraightLength(currentPiece.getStraightLength() + prevPiece.getLength());
                    currentSequence = prevPiece.getStraightLength();
                    currentSeqBeginning = currentTrackIndex;
                }
            } else {
                // The straight sequence is ending
                if (prevSequence < currentSequence) {
                    prevSequence = currentSequence;
                    prevSequenceBeginning = currentSeqBeginning;
                }
                currentSeqBeginning = -1;
                currentSequence = 0;
            }

            currentPiece = prevPiece;
            currentTrackIndex --;
            if (currentTrackIndex < 0) {
                currentTrackIndex = track.getPieces().size() - 1;
            }
            if (currentTrackIndex == firstCurvedPiece)
                break;
        }

        if (prevSequence < currentSequence) {
            prevSequence = currentSequence;
            prevSequenceBeginning = currentSeqBeginning;
        }

        track.getPieces().get(prevSequenceBeginning).setPotentialTurbo(true);

        for (int i = 0; i< track.getLanes().size(); i++) {
            currentSeqBeginning = -1;
            currentPiece = getPreviousPiece(firstCurvedPiece);
            if (currentPiece.getAngle() > 0) {
                currentPiece.setCurvedLength(i, currentPiece.getComputedLaneLength(i));
                currentPiece.setAngleRemaining(currentPiece.getAngle());
            }

            currentTrackIndex = firstCurvedPiece - 1;
            if (currentTrackIndex < 0) {
                currentTrackIndex = track.getPieces().size() - 1;
            }

            while (true) {
                TrackPiece prevPiece = getPreviousPiece(currentTrackIndex);
                // Is it curved
                if (prevPiece.getAngle() != 0) {
                    // Is this a new sequence or the radius is different
                    if ((currentSeqBeginning == -1) ||
                            (prevPiece.getComputedLaneRadius(i) != currentPiece.getComputedLaneRadius(i)) ||
                            (Math.abs(prevPiece.getAngle() + currentPiece.getAngle()) < Math.abs(currentPiece.getAngle()) + Math.abs(prevPiece.getAngle()) )) {
                        prevPiece.setCurvedLength(i, prevPiece.getComputedLaneLength(i));
                        prevPiece.setAngleRemaining(prevPiece.getAngle());
                        currentSeqBeginning = currentTrackIndex;
                    } else {
                        prevPiece.setCurvedLength(i, currentPiece.getCurvedLength(i) + prevPiece.getComputedLaneLength(i));
                        prevPiece.setAngleRemaining(currentPiece.getAngleRemaining() + prevPiece.getAngle());
                    }
                } else {
                    // The curvy sequence is ending
                    currentSeqBeginning = -1;
                }
                currentPiece = prevPiece;
                currentTrackIndex --;
                if (currentTrackIndex < 0) {
                    currentTrackIndex = track.getPieces().size() - 1;
                }
                if (currentTrackIndex == firstCurvedPiece)
                    break;
            }
        }
    }

    private TrackPiece getPreviousPiece(int index) {
        if (index == 0) {
            index = gameData.getRace().getTrack().getPieces().size();
        }
        index --;

        return gameData.getRace().getTrack().getPieces().get(index);
    }

    @Override
    public void handleGameStart(String gameId, int gameTick) {
        if (gameData.getRace().getRaceSession().getQuickRace() != null &&
                gameData.getRace().getRaceSession().getQuickRace()) {
            // No qualifying. Get running now
            inQualifying = false;
        } else {
            // If I am already in qualifying, I am now racing
            if (inQualifying) {
                inQualifying = false;
            } else {
                inQualifying = true;
            }
        }
        CarControl carControl = carInfo.getCarControl(reactor, false);
        executeCarControl(gameId, gameTick, carControl);
    }

    @Override
    public void handleCarPositions(CarPositions carPositions, String gameId, Integer gameTick) {
        boolean preStartPosition = false;
        if (gameTick == null) {
            gameTick = 0;
            preStartPosition = true;
        }

        CarPosition myCar = carPositions.findMatchingCar(carInfo.getMyCarIdentity());
        allCarDetails.updateCarPositions(carPositions, gameTick);



        if (inCrash) {
            System.out.println("In Crash.");
            sendPing();
        } else {
            carInfo.updatePosition(myCar, gameTick);
            CarControl carControl = carInfo.getCarControl(reactor, preStartPosition);
            if (preStartPosition) {
                System.out.println("Not going to send any response now!");
                sendPing();
            } else {
                executeCarControl(gameId, gameTick, carControl);
            }
            carInfo.log();
            carInfo.finishUpdate();
        }

    }

    private void executeCarControl(String gameId, Integer gameTick, CarControl carControl) {
        switch (carControl.getAction()) {
            case SWITCH_LANE:
                carInfo.updateSwitchOrder(carControl.getSwitchLane());
                switch (carControl.getSwitchLane()) {
                    case LEFT:
                        botWriter.send(MessageType.SWITCH_LANE, "Left");
                        break;
                    case RIGHT:
                        botWriter.send(MessageType.SWITCH_LANE, "Right");
                        break;
                    case STAY:
                }
                break;
            case APPLY_TURBO:
                turboBoost();
                break;
            case THROTTLE:
                botWriter.throttle(carControl.getThrottle(), gameId, gameTick);
                break;
            case PING:
                sendPing();
                break;
        }
    }

    @Override
    public void handleGameEnd(GameEnd gameEnd) {
//        reactor.shutdown();
//        reactor = null;
        reactor.setSpeedTrained();
    }

    @Override
    public void sendPing() {
        botWriter.send(MessageType.PING, null);
    }

    @Override
    public void handleCrash(CarIdentity data, String gameId, Integer gameTick) {
        allCarDetails.carCrashed(data);


        if (data.getName().equals(carInfo.getMyCarIdentity().getName())) {
            sendPing();
            carInfo.updateCrash();
            reactor.updateCrash();
            inCrash = true;
        }
    }

    @Override
    public void handleSpawn(CarIdentity data, String gameId, Integer gameTick) {
        allCarDetails.carSpawned(data);
        if (data.getName().equals(carInfo.getMyCarIdentity().getName())) {
            inCrash = false;
            reactor.updateSpawn();
        }
    }

    @Override
    public void handleLapFinished(LapFinished data) {
    }

    @Override
    public void shutdown() {
        System.exit(0);
    }

    @Override
    public void handleDnf(CarIdentity data) {
    }

    @Override
    public void handleTournamentEnd() {
        shutdown();
    }

    @Override
    public void handleFinish(CarIdentity data) {

    }

    @Override
    public void sendJoinRace(CreateRace race) {
        botWriter.send(MessageType.JOIN_RACE, race);
    }

    @Override
    public void handleTurboAvailable(TurboAvailable turboData) {
        allCarDetails.turboAvailable(turboData);
        this.turboData.add(turboData);
    }

    @Override
    public void handleTurboStart(CarIdentity car, Integer gameTick) {
        allCarDetails.turboStarted(car);

        if (car.getColor().equals(myCar.getColor())) {
            carInfo.setTurboApplied(this.turboData.get(0));
            carInfo.startTurbo(this.turboData.remove(0));
            carInfo.setTurboStartTime(gameTick);
        }
        turboBlocked--;
    }

    @Override
    public void handleTurboEnd(CarIdentity car, Integer gameTick) {
        allCarDetails.turboEnd(car);
        carInfo.setTurboApplied(null);
        if (car.getColor().equals(myCar.getColor())) {
            carInfo.endTurbo();
        }

    }

    private void turboBoost() {
        if ((turboData.size() - turboBlocked) > 0 ) {
            allCarDetails.turboApplied(myCar);
            botWriter.send(MessageType.TURBO, "Whee!!!");
            turboBlocked++;
        }
    }
}
