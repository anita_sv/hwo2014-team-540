package noobbot;

import com.google.inject.Guice;
import com.google.inject.Injector;
import noobbot.msg.CreateRace;
import noobbot.msg.Join;
import noobbot.wire.BotEngine;

import java.io.*;
import java.net.Socket;

public class Main {

    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
        String trackName = "RANDOM";
        if (args.length > 4) {
            trackName = args[4];
        }

        if (args.length > 5) {
            int maxDrift = Integer.parseInt(args[5]);
            ReactorModule.setMaxDrift(maxDrift);
        }



        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey + " to track " + trackName);

        Socket socket = new Socket(host, port);
        PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        Main main = new Main(reader, writer);

        Join botData = new Join(botName, botKey);

        if ("RANDOM".equals(trackName)) {
            main.quickRace(botData);
        } else {
            main.createAndJoin(new CreateRace(botData, trackName, "Inmobi", 1));
        }
        System.exit(0);
    }


    private final NoobBot noobBot;

    private final BotEngine botEngine;

    public Main(final BufferedReader reader, PrintWriter writer)  {

        Injector injector = Guice.createInjector(new NoobBotModule(reader, writer));

        this.noobBot = injector.getInstance(NoobBot.class);
        this.botEngine = injector.getInstance(BotEngine.class);
    }

    public void quickRace(Join join) throws IOException {
        this.noobBot.init(join);
        botEngine.loop();
    }

    public void createAndJoin(CreateRace race) throws IOException {
        this.noobBot.init(race);
        botEngine.loop();
    }

}

