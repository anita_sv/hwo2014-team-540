package noobbot.data;

import lombok.Data;

/**
 * Created by anita on 4/22/14.
 */
@Data
public class PositionMetric {

    private final double position;

    private final double velocity;

    private final double acceleration;
}
