package noobbot.data;

/**
* Created by anita on 4/25/14.
*/
public enum Action {
    SWITCH_LANE,
    APPLY_TURBO,
    THROTTLE,
    PING
}
