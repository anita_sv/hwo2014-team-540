package noobbot.data;

import lombok.Data;

/**
 * Created by anita on 4/22/14.
 */
@Data
public class FullMetric {

    private final PositionMetric positionMetric;

    private final DriftMetric driftMetric;

    private final ThrottleMetric throttleMetric;

    private final double signedCurvature;

    private final int stateSpaceIndex;
}
