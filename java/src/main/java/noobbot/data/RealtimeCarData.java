package noobbot.data;

import lombok.Data;
import noobbot.msg.TurboAvailable;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by vaidhy.gopalan on 25/04/14.
 */

@Data
public class RealtimeCarData {
    private String color;
    private int lap;
    private int pieceIndex;
    private double inPieceDistance;
    private double velocity;
    private double drift;
    private double driftRate;
    private int lane;
    private int turboAvailable;
    private double sumOfVelocity;
    private int ticks;
    private int numCrashes;

    private List<Double> velocityLastFewTicks = new LinkedList<Double>();
    private List<Double> driftRateLastFewTicks = new LinkedList<Double>();

    private boolean inCrash;
    private boolean inTurbo;


    private List<TurboAvailable> turbos = new ArrayList<>();

    public double getCurrentVelocity() {
        double currentVelocity = 0;
        for (Double d : velocityLastFewTicks) {
            currentVelocity += d;
        }
        return currentVelocity/5;
    }

    public double getCurrentDriftRate() {
        double currentDriftRate = 0;
        for (Double d : driftRateLastFewTicks) {
            currentDriftRate += d;
        }
        return currentDriftRate/5;
    }

    public void updateVelocity(double velocity) {
        this.velocity = velocity;
        sumOfVelocity += velocity;
        if (velocityLastFewTicks.size() == 5) {
            velocityLastFewTicks.remove(0);
        }
        velocityLastFewTicks.add(velocity);
    }

    public void updateDriftRate(double driftRate) {
        this.driftRate = driftRate;
        if (driftRateLastFewTicks.size() == 5) {
            driftRateLastFewTicks.remove(0);
        }
        driftRateLastFewTicks.add(driftRate);
    }
}

