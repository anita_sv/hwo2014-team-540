package noobbot.data;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import noobbot.msg.*;
import noobbot.physics.GamePhysics;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by vaidhy.gopalan on 25/04/14.
 */
@RequiredArgsConstructor
public class Cars {

    private final List<TrackPiece> trackPiecesList;
    private final CarDimensions carDimensions;
    private final GamePhysics gamePhysics;

    private int MAX_LOOKAHEAD_PIECES = 5;

    private CarIdentity identity;
    private Map<String, RealtimeCarData> allCars = new HashMap<>();

    private int myPrevPieceIndex = 0;

    private double maxDrift = 0.0;

    @Data
    public static class SomeCar {
        private final String color;
        private final double distance;
    }

    public void setMyCarIdentity(CarIdentity myCar) {
        this.identity = myCar;
    }

    public RealtimeCarData getOtherCar(String color) {
        return allCars.get(color);
    }

    public void updateCarPositions(CarPositions positions, int gameTick) {
        boolean isThisMyCar = false;
        for (CarPosition position : positions.getCarPositionList()) {
            RealtimeCarData data = allCars.get(position.getIdentity().getColor());
            if (data == null) {
                data = new RealtimeCarData();
                data.setColor(position.getIdentity().getColor());
                allCars.put(position.getIdentity().getColor(), data);
            }

            if (position.getIdentity().getColor().equals(identity.getColor())) {
                isThisMyCar = true;
            }

            PiecePosition pp = position.getPiecePosition();

            // skip crashed cars
            if (data.isInCrash()) {
                continue;
            }

            double distanceCovered = 0;
            if (data.getLap() == pp.getLap()) {
                if (data.getPieceIndex() == pp.getPieceIndex()) {
                    distanceCovered = pp.getInPieceDistance() - data.getInPieceDistance();
                } else {
                    distanceCovered = pp.getInPieceDistance();
                    distanceCovered += trackPiecesList.get(data.getPieceIndex()).getComputedLaneLength(pp.getLane().getStartIndex()) - data.getInPieceDistance();
                }
            } else {
                // Assuming we are on the first piece after the lap is over.
                distanceCovered = pp.getInPieceDistance();
                distanceCovered += trackPiecesList.get(data.getPieceIndex()).getComputedLaneLength(pp.getLane().getStartIndex()) - data.getInPieceDistance();
            }

            int tickDiff = gameTick - data.getTicks();
            data.updateVelocity(distanceCovered / (tickDiff));
            data.setTicks(gameTick);
            data.setLap(pp.getLap());
            data.setPieceIndex(pp.getPieceIndex());
            data.setInPieceDistance(pp.getInPieceDistance());
            data.setLane(pp.getLane().getEndIndex());

            double prevDrift = data.getDrift();
            data.setDrift(position.getAngle());
            data.updateDriftRate((position.getAngle() - prevDrift)/(tickDiff));

            if (isThisMyCar) {
                if (myPrevPieceIndex != pp.getPieceIndex()) {
                    TrackPiece prevPiece = trackPiecesList.get(myPrevPieceIndex);
                    TrackPiece currPiece = trackPiecesList.get(pp.getPieceIndex());

                    if (prevPiece.getRadius() > 0) {
                        gamePhysics.getCurveVelocityModel().updatePieceAtEnd(myPrevPieceIndex,
                                pp.getLane().getStartIndex(),maxDrift);
                    }

                    maxDrift = 0.0;
                    myPrevPieceIndex = pp.getPieceIndex();
                }

                if (Math.abs(data.getDrift()) > Math.abs(maxDrift)) {
                    maxDrift = data.getDrift();
                }
            }
        }
    }

    public void carCrashed(CarIdentity car) {
        RealtimeCarData data = allCars.get(car.getColor());
        data.setInCrash(true);
        data.setNumCrashes(data.getNumCrashes() + 1);

        if (car.getColor().equals( myCar().getColor())) {
            gamePhysics.getCurveVelocityModel().updateCrash(myCar().getPieceIndex(),
                    myCar().getLane(),
                    myCar().getInPieceDistance());
        }
    }

    public void carSpawned(CarIdentity car) {
        RealtimeCarData data = allCars.get(car.getColor());
        data.setInCrash(false);
    }

    public void turboAvailable(TurboAvailable turbo) {
        for (RealtimeCarData carz : allCars.values()) {
            carz.setTurboAvailable(carz.getTurboAvailable() + 1);
            carz.getTurbos().add(turbo);
        }
    }

    public void turboApplied(CarIdentity car) {
        RealtimeCarData data = allCars.get(car.getColor());
        data.setInTurbo(true);
    }

    public void turboStarted(CarIdentity car) {
        RealtimeCarData data = allCars.get(car.getColor());
        data.setTurboAvailable(data.getTurboAvailable() - 1);
    }

    public void turboEnd(CarIdentity car) {
        RealtimeCarData data = allCars.get(car.getColor());
        data.setInTurbo(false);
    }

    // Will only return value if the car is only a short distance ahead i.e., within 5 pieces
    public SomeCar getCarInfront() {

        RealtimeCarData myCar = allCars.get(identity.getColor());

        double distance = 0;
        String keyColor = null;


        for (String color : allCars.keySet()) {
            // Skip my car
            if (color.equals(identity.getColor())) {
                continue;
            }

            RealtimeCarData otherCar = allCars.get(color);
            double trackPieceLength = trackPiecesList.get(myCar.getPieceIndex()).getComputedLaneLength(myCar.getLane());
            double tempDistance = 0;
            String tempColor = null;

            int startPieceIndex = myCar.getPieceIndex();
            int endPieceIndex = myCar.getPieceIndex() + MAX_LOOKAHEAD_PIECES;
            if (endPieceIndex >= trackPiecesList.size()) {
                endPieceIndex = endPieceIndex % trackPiecesList.size();
            }

            // Are we on the same lane
            if (otherCar.getLane() == myCar.getLane()) {
                // Are we close
                if ((otherCar.getPieceIndex() >= startPieceIndex) &&
                        (otherCar.getPieceIndex() <= endPieceIndex)) {

                    if (myCar.getPieceIndex() == otherCar.getPieceIndex()) {
                        tempDistance = otherCar.getInPieceDistance() - myCar.getInPieceDistance();
                        tempColor = color;
                    } else {
                        int tempIndex = otherCar.getPieceIndex();
                        tempDistance = otherCar.getInPieceDistance();
                        tempDistance += trackPieceLength - myCar.getInPieceDistance();

                        startPieceIndex++;
                        while (startPieceIndex != endPieceIndex) {
                            if (startPieceIndex == trackPiecesList.size()) {
                                startPieceIndex = 0;
                            }
                            tempDistance += trackPiecesList.get(startPieceIndex).getComputedLaneLength(otherCar.getLane());
                            startPieceIndex++;
                        }
                    }
                }
            }

            if (tempDistance < distance) {
                distance = tempDistance;
                keyColor = tempColor;
            }
        }

        if (distance > 0) {
            return new SomeCar(keyColor, distance);
        }

        return null;
    }

    public boolean isCarBlocked() {
        SomeCar otherCar = getCarInfront();
        if ((otherCar != null) && (otherCar.distance < carDimensions.getLength()*2)) {
            return true;
        }
        return false;
    }

    public int getLaneCongestion(int lane, int lookAhead) {
        int result = 0;
        for (String color : allCars.keySet()) {
            // Skip my car
            if (color.equals(identity.getColor())) {
                continue;
            }

            RealtimeCarData otherCar = allCars.get(color);
            int startPieceIndex = myCar().getPieceIndex();
            int endPieceIndex = startPieceIndex + lookAhead;
            if (endPieceIndex >= trackPiecesList.size()) {
                endPieceIndex -= trackPiecesList.size();
            }
            // FIXME: Across lane boundary it is incorrect.
            if ((otherCar.getLane() == lane) &&
                    (otherCar.getPieceIndex() >= startPieceIndex) &&
                    (otherCar.getPieceIndex() <= endPieceIndex)){
                result++;
            }
        }
        return result;
    }

    public boolean isCarInFrontSpeeding() {
        SomeCar carInfront = getCarInfront();
        if (carInfront == null) {
            return false;
        }

        RealtimeCarData otherCar = allCars.get(carInfront.getColor());
        RealtimeCarData myCar = allCars.get(identity.getColor());

        if (myCar.getVelocity() < otherCar.getCurrentVelocity()) {
            return true;
        }

        return false;
    }

    public RealtimeCarData myCar() {
        return allCars.get(identity.getColor());
    }
}

