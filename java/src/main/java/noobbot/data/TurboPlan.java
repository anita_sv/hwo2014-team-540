package noobbot.data;

import lombok.Data;
import noobbot.msg.PiecePosition;

/**
 * Created by anita on 4/24/14.
 */
@Data
public class TurboPlan {

    private final PiecePosition position;

    private final double turboFactor;

    private final int turboDurationTicks;
}
