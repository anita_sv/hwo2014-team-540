package noobbot.data;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * Created by anita on 4/25/14.
 */
@Data
public class PredictivePlan {

    private final List<FullMetric> fullMetricList;

    // Which lane do you want to be in.
    private final Map<Integer, Integer> laneTarget;

    private final List<TurboPlan> turboPlanList;
}
