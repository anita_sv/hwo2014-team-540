package noobbot.data;

import lombok.Getter;

/**
 * Created by anita on 4/25/14.
 */
@Getter
public class CarControl {

    private final Action action;

    private final SwitchLane switchLane;

    private final Double throttle;

    private CarControl(Action action,
                       SwitchLane switchLane,
                       Double throttle) {
        this.action = action;
        this.switchLane = switchLane;
        this.throttle = throttle;
    }

    public static CarControl switchLeft() {
        return new CarControl(Action.SWITCH_LANE, SwitchLane.LEFT, null);
    }

    public static CarControl switchRight() {
        return new CarControl(Action.SWITCH_LANE, SwitchLane.RIGHT, null);
    }

    public static CarControl applyTurbo() {
        return new CarControl(Action.APPLY_TURBO, null, null);
    }

    public static CarControl ping() {
        return  new CarControl(Action.PING, null, null);
    }

    public static CarControl applyThrottle(double throttle) {
        if (throttle < 0) {
            throttle = 0;
        }
        if (throttle > 1) {
            throttle = 1;
        }
        return new CarControl(Action.THROTTLE, null, throttle);
    }

    public static CarControl brake() {
        return new CarControl(Action.THROTTLE, null, 0.0);
    }


}
