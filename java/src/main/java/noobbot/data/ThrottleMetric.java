package noobbot.data;

import lombok.Data;
import noobbot.msg.TurboAvailable;

/**
 * Created by anita on 4/22/14.
 */
@Data
public class ThrottleMetric {

    private final double throttle;

    private final TurboAvailable turboAvailable;

    private final int throttleStartTick;
}
