package noobbot.data;

import lombok.Data;

/**
 * Created by anita on 4/22/14.
 */
@Data
public class DriftMetric {

    private final double driftDegrees;

    private final double driftRateDegrees;

    private final double driftAccelDegrees;
}
