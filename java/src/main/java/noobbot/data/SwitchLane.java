package noobbot.data;

/**
* Created by anita on 4/25/14.
*/
public enum SwitchLane {
    LEFT,
    RIGHT,
    STAY
}
