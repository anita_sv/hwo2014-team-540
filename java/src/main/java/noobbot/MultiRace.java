package noobbot;

import com.google.inject.Guice;
import com.google.inject.Injector;
import noobbot.msg.CreateRace;
import noobbot.msg.Join;
import noobbot.wire.BotEngine;

import java.io.*;
import java.net.Socket;

/**
 * Created by vaidhy.gopalan on 24/04/14.
 */
public class MultiRace {

    public static void main(String... args) throws IOException {
        String host = "prost.helloworldopen.com";
        int port = 8091;
        String botKey = "8GUvPw+sqe5hcA";

        String command = args[0];
        String botName = args[1];
        String password = args[2];
        int numCars = Integer.parseInt(args[3]);
        String trackName = args[4];


        int maxDrift = 50 + (int)(Math.random() * 100)%10;
        ReactorModule.setMaxDrift(maxDrift);

        botName = String.format("%s_D_%d", botName, maxDrift);

        if (password.equals("")){
            password = null;
        }

        System.out.println(command + " race with name " + botName + " on track " + trackName);

        Socket socket = new Socket(host, port);
        PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        MultiRace main = new MultiRace(reader, writer);

        Join botData = new Join(botName, botKey);

        if (command.equals("create")) {
            main.createAndJoin(new CreateRace(botData, trackName, password, numCars));
        } else if (command.equals("join")) {
            main.joinRace(new CreateRace(botData, trackName, password, numCars));
        }
    }


    private final NoobBot noobBot;

    private final BotEngine botEngine;

    public MultiRace(final BufferedReader reader, PrintWriter writer) {

        Injector injector = Guice.createInjector(new NoobBotModule(reader, writer));

        this.noobBot = injector.getInstance(NoobBot.class);
        this.botEngine = injector.getInstance(BotEngine.class);
    }

    public void createAndJoin(CreateRace race) throws IOException {
        this.noobBot.init(race);
        botEngine.loop();
    }

    public void joinRace(CreateRace race) throws IOException {
        this.noobBot.join(race);
        botEngine.loop();
    }

}
