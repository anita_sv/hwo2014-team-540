package noobbot.trainer;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import noobbot.controller.PhysicsStore;
import noobbot.data.FullMetric;
import noobbot.physics.*;
import noobbot.planner.StandardPredictive;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by anita on 4/26/14.
 */
public class PhysicsTrainer {

    private final PhysicsStore physicsStore;

    private volatile boolean dataCollectionStatus;

    private final List<FullMetric> fullMetricList =
            new ArrayList<>();

    private final StandardPredictive standardPredictive;

    private int finalMaxDrift;

    private boolean speedTrained = false;

    @Inject
    public PhysicsTrainer(PhysicsStore physicsStore,
                          @Named("initialPhysics") GamePhysics initialPhysics,
                          StandardPredictive standardPredictive,
                          @Named("finalMaxDrift") int finalMaxDrift) {
        this.physicsStore = physicsStore;
        this.standardPredictive = standardPredictive;
        this.finalMaxDrift = finalMaxDrift;
        this.dataCollectionStatus = true;
        physicsStore.set(initialPhysics);
    }

    public void recordMetric(FullMetric fullMetric) {
        if (dataCollectionStatus) {
            this.fullMetricList.add(fullMetric);

            if (fullMetricList.size() >= 100 &&
                    fullMetricList.size() % 50 == 0) {
                train();
            }
        }
    }

    public void train() {
//        return;
        System.out.println("Training Requested");
        if (fullMetricList.size() < 5) {
            System.out.println("Size: " + fullMetricList.size() + "No Metrics gotten so far, can't train");
        }
        try {

            SpeedModel sm = physicsStore.get().getDefaultPhysics().getSpeedModel();;
            if (!speedTrained) {
                SpeedModel newSm = SpeedModelUtil.train(fullMetricList);
                if (newSm.getError() < sm.getError()) {
                    sm = newSm;
                }
            }
            CurveVelocityModel2 curveVelocityModel = physicsStore.get().getCurveVelocityModel();

            // DriftModel dm = DriftModelUtil.train(fullMetricList);
            DriftModel dm = new DriftModel(new DriftModel.ModelData(0.84, 0.11, 0.20, 0));
            GamePhysicsMap defaultGamePhysics = new GamePhysicsMap(sm, dm);

            ImmutableMap.Builder<Integer, GamePhysicsMap> gamePhysicsAtlas = ImmutableMap.builder();

            Map<Integer, List<FullMetric> > fullMetricListMap = new HashMap<>();

            for (FullMetric fullMetric : fullMetricList) {
                int state = fullMetric.getStateSpaceIndex();
                if (fullMetricListMap.containsKey(state)) {
                    fullMetricListMap.get(state).add(fullMetric);
                } else {
                    List<FullMetric> stateList = new ArrayList<>();
                    stateList.add(fullMetric);
                    fullMetricListMap.put(state, stateList);
                }
            }

            GamePhysics gamePhysics = new GamePhysics(defaultGamePhysics,
                   gamePhysicsAtlas.build(), curveVelocityModel);
            physicsStore.set(gamePhysics);
            standardPredictive.setMaxDrift(finalMaxDrift);
        } catch (RuntimeException e) {
            System.out.println("Training Failed");
            e.printStackTrace();
            return;
        }
    }


    public void setSpeedTrained() {
        this.speedTrained = true;
    }

    public void enableDataCollection() {
        dataCollectionStatus = true;
    }

    public void disableDataCollection() {
        dataCollectionStatus = false;
    }
}
