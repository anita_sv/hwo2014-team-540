package noobbot.controller;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import noobbot.data.CarControl;
import noobbot.data.FullMetric;
import noobbot.data.PredictivePlan;
import noobbot.data.TurboPlan;

/**
 * Created by anita on 4/25/14.
 */
@Singleton @Getter
public class PlanStore {

    private volatile PredictivePlan plan;

    @Inject
    public PlanStore() {
        this.plan = new PredictivePlan(ImmutableList.<FullMetric>of(),
                ImmutableMap.<Integer, Integer>of(),
                ImmutableList.<TurboPlan>of());
    }

    public void setPlan(PredictivePlan plan) {
        if (plan != null) {
            Preconditions.checkNotNull(plan.getTurboPlanList());
            this.plan = plan;
        }
    }
}
