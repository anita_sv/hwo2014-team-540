package noobbot.controller;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import noobbot.physics.SolvingParams;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by anita on 4/26/14.
 */
@Singleton
public class SolvingThread {

    private final ExecutorService singleThread;

    private final SolvingActivity solvingActivity;

    private int urgentDepth;

    private int blockingDepth;

    private SolvingParams lastSolvingParams = null;
    private SolvingParams lastReceivedParams = null;
    private Future<Boolean> lastSolvingFuture = null;

    private final int initialDepth;

    @Inject
    public SolvingThread(SolvingActivity solvingActivity,
                         @Named("initialDepth") int initialDepth,
                         @Named("urgentDepth") int urgentDepth,
                         @Named("blockingDepth") int blockingDepth) {
        this.solvingActivity = solvingActivity;
        this.urgentDepth = urgentDepth;
        this.blockingDepth = blockingDepth;
        this.initialDepth = initialDepth;
        singleThread = Executors.newSingleThreadExecutor();
    }

    public void setSolvingParams(SolvingParams solvingParams) {
        lastReceivedParams = solvingParams;

        if (lastSolvingParams != null &&
                (lastSolvingParams.getPiecePosition().getPieceIndex() !=
                solvingParams.getPiecePosition().getPieceIndex()) &&
                !lastSolvingFuture.isDone() &&
                !lastSolvingFuture.isCancelled()) {
            lastSolvingFuture.cancel(true);
            startNow(initialDepth);
        } else if (lastSolvingParams == null) {
            startNow(initialDepth);
        }

    }

    public void startNow(int depth) {
        Runnable action = solvingActivity.newSolver(lastReceivedParams, depth);
        lastSolvingParams = lastReceivedParams;
        lastSolvingFuture = singleThread.submit(action, true);

    }

    public void startUrgent() {
        startNow(urgentDepth);
    }

    public boolean blockingSolve() throws InterruptedException, ExecutionException {
        if (lastSolvingFuture != null &&
                (!lastSolvingFuture.isDone() && !lastSolvingFuture.isCancelled())) {
            lastSolvingFuture.cancel(true);
        }
        Runnable action = solvingActivity.newSolver(lastReceivedParams, blockingDepth);
        action.run();
        return true;
    }

    public void shutdown() {
        singleThread.shutdownNow();
    }
}
