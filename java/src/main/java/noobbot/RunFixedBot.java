package noobbot;

import com.google.inject.Guice;
import com.google.inject.Injector;
import noobbot.msg.CreateRace;
import noobbot.msg.Join;
import noobbot.wire.BotEngine;

import java.io.*;
import java.net.Socket;

/**
 * Created by vaidhy.gopalan on 24/04/14.
 */
public class RunFixedBot {

    public static void main(String... args) throws IOException {
        String host = "prost.helloworldopen.com";
        int port = 8091;
        String botKey = "8GUvPw+sqe5hcA";

        String command = args[0];
        String botName = args[1];
        String password = args[2];
        int numCars = Integer.parseInt(args[3]);
        String trackName = args[4];

        int maxDrift = 30 + (int)(Math.random() * 100)%30;
        ReactorModule.setMaxDrift(maxDrift);

        double throttle_factor = (double)((int)(Math.random() * 10)%5)/100;
        botName = String.format("%s_D_%d_tf_%.2f", botName, maxDrift, (1.0 - throttle_factor));

        if (password.equals("")){
            password = null;
        }

        System.out.println(command + " race with name " + botName + " on track " + trackName);

        Socket socket = new Socket(host, port);
        PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        MultiRace main = new MultiRace(reader, writer);

        Join botData = new Join(botName, botKey);

        if (command.equals("create")) {
            main.createAndJoin(new CreateRace(botData, trackName, password, numCars));
        } else if (command.equals("join")) {
            main.joinRace(new CreateRace(botData, trackName, password, numCars));
        }
    }


    private final FixedBot fixedBot;

    private final BotEngine botEngine;

    public RunFixedBot(final BufferedReader reader, PrintWriter writer) {

        Injector injector = Guice.createInjector(new NoobBotModule(reader, writer));

        this.fixedBot = injector.getInstance(FixedBot.class);
        this.botEngine = injector.getInstance(BotEngine.class);
    }

    public void createAndJoin(CreateRace race) throws IOException {
        this.fixedBot.init(race);
        botEngine.loop();
    }

    public void joinRace(CreateRace race) throws IOException {
        this.fixedBot.join(race);
        botEngine.loop();
    }

}
