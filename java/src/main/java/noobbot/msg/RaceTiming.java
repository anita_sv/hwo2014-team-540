package noobbot.msg;

import lombok.Data;

/**
 * Created by anita on 4/18/14.
 */
@Data
public class RaceTiming {

    private final Integer laps;

    private final Integer ticks;

    private final Integer millis;

}
