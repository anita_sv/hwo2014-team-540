package noobbot.msg;

import lombok.Data;

/**
 * Created by vaidhy.gopalan on 21/04/14.
 */
@Data
public class TurboAvailable {
    private final double turboDurationMilliseconds;
    private final int turboDurationTicks;
    private final double turboFactor;
}
