package noobbot.msg;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

/**
 * Created by vaidhy on 19/4/14.
 */
@Data
public class CreateRace {
    @SerializedName("botId")
    private final Join joinData;

    private final String trackName;
    private final String password;

    private final int carCount;
}
