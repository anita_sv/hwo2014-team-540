package noobbot.msg;

import lombok.Data;

/**
 * Created by anita on 4/17/14.
 */
@Data
public class LapFinished {

    private final CarIdentity car;

    private final LapTiming lapTime;

    private final RaceTiming raceTime;

    private final Ranking ranking;
}
