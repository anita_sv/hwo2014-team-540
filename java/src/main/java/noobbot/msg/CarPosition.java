package noobbot.msg;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

/**
 */
@Data
public class CarPosition {

    @Data
    public static class LanePosition {
        @SerializedName("startLaneIndex")
        private final int startIndex;

        @SerializedName("endLaneIndex")
        private final int endIndex;
    }

    @SerializedName("id")
    private final CarIdentity identity;

    private final double angle;

    private final PiecePosition piecePosition;
}
