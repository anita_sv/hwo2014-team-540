package noobbot.msg;

import lombok.Data;

/**
 * Created by anita on 4/17/14.
 */
@Data
public class Position {

    private final double x;

    private final double y;
}
