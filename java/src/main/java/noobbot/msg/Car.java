package noobbot.msg;

import lombok.Data;

/**
 */
@Data
public class Car {

    private final CarIdentity id;

    private final CarDimensions dimensions;
}
