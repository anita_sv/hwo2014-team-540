package noobbot.msg;

import lombok.Data;

/**
 */
@Data
public class Join {

    private final String name;

    private final String key;
}
