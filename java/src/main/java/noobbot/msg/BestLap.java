package noobbot.msg;

import lombok.Data;

/**
 * Created by anita on 4/18/14.
 */
@Data
public class BestLap {

    private final CarIdentity car;

    private final LapTiming result;

}
