package noobbot.msg;

import lombok.Getter;
import lombok.experimental.Builder;

/**
* Created by anita on 4/25/14.
*/
@Getter
@Builder
public class PiecePosition {
    private final int pieceIndex;
    private final double inPieceDistance;
    private final CarPosition.LanePosition lane;
    private final int lap;
}
