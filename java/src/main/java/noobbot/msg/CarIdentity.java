package noobbot.msg;

import lombok.Data;

/**
* Created by anita on 4/17/14.
*/
@Data
public class CarIdentity {
    private final String name;
    private final String color;
}
