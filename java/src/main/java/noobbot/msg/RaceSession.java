package noobbot.msg;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

/**
 * Created by anita on 4/17/14.
 */
@Data
public class RaceSession {

    private final Integer laps;

    private final Integer maxTimeMs;

    private final Boolean quickRace;

    private int actualLaps;
}
