package noobbot.msg;

import lombok.Data;

/**
 */
@Data
public class GameInit {

    private final Race race;
}
