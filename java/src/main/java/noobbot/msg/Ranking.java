package noobbot.msg;

import lombok.Data;

/**
 * Created by anita on 4/18/14.
 */
@Data
public class Ranking {

    private final int overall;

    private final int fastestLap;
}
