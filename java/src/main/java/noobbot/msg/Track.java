package noobbot.msg;

import lombok.Data;

import java.util.List;

/**
 * Created by anita on 4/17/14.
 */
@Data
public class Track {

    private final String id;

    private final String name;

    private final List<TrackPiece> pieces;

    private final List<TrackLane> lanes;

    private final Orientation startingPoint;
}
