package noobbot.msg;

import lombok.Data;

/**
 * Created by anita on 4/17/14.
 */
@Data
public class Orientation {

    private final Position position;

    private final Double angle;
}
