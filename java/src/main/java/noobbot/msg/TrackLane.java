package noobbot.msg;

import lombok.Data;

/**
 * Created by anita on 4/17/14.
 */
@Data
public class TrackLane {

    private final double distanceFromCenter;

    private final int index;
}
