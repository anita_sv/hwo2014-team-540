package noobbot.msg;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by anita on 4/17/14.
 */
@Data
public class TrackPiece {

    private final double length;

    @SerializedName("switch")
    private final boolean switchLane;

    private final double angle;

    private final int radius;

    private transient int index;

    // Store lane lengths as LaneIndex:Length
    private transient Map<Integer, Double> computedLaneLength;

    private transient Map<Integer, Double> computedLaneRadius;

    private transient Map<Integer, Double> computedCurvature;

    private transient Map<Integer, Orientation> startOrientation;

    private transient Map<Integer, Orientation> stopOrientation;

    private transient Map<Integer, Double> curvedLength;

    private transient int direction = 0;

    private transient boolean potentialTurbo = false;

    private transient int potentialTurboLap = 0;

    private transient double straightLength = 0;

    private transient double angleRemaining = 0;


    public double getComputedLaneLength(int laneIndex) {
        return computedLaneLength.get(laneIndex);
    }

    public void setComputedLaneLength(int laneIndex,double length) {
        if (computedLaneLength == null) {
            computedLaneLength = new HashMap<>();
        }
        computedLaneLength.put(laneIndex, length);
    }

    public double getCurvedLength(int laneIndex) {
        if (curvedLength == null) {
            return 0.00;
        }
        return curvedLength.get(laneIndex);
    }

    public void setCurvedLength(int laneIndex, double length) {
        if (curvedLength == null) {
            curvedLength = new HashMap<>();
        }
        curvedLength.put(laneIndex, length);
    }

    public double getComputedLaneRadius(int laneIndex) {
        return computedLaneRadius.get(laneIndex);
    }

    public double getComputedCurvature(int laneIndex) {
        return computedCurvature.get(laneIndex);
    }

    public void setComputedLaneRadius(int laneIndex,double radius) {
        if (computedLaneRadius == null) {
            computedLaneRadius = new HashMap<>();
            computedCurvature = new HashMap<>();
        }
        computedLaneRadius.put(laneIndex, radius);

        if (radius > 0) {
            if (angle > 0) {
                direction = 1;
            } else {
                direction = -1;
            }
            computedCurvature.put(laneIndex, direction * 1.0 / radius);
        } else  {
            computedCurvature.put(laneIndex, 0.0);
            direction = 0;
        }
    }


    @Override
    public String toString(){
        return String.format("Length : %.3f, Radius: %d, angle: %.1f", length, radius, angle);
    }
}
