package noobbot.msg;

import lombok.Data;

/**
 */
@Data
public class CarDimensions {

    private final double length;

    private final double width;

    private final double guideFlagPosition;
}
