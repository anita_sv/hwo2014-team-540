package noobbot.adaptive;

import com.google.inject.ImplementedBy;
import noobbot.data.CarControl;
import noobbot.physics.SolvingParams;

/**
 * Created by anita on 4/25/14.
 */
@ImplementedBy(StandardAdaptive.class)
public interface Adaptive {

    CarControl getCarControl(SolvingParams solvingParams);
}
