package noobbot;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Guice;
import com.google.inject.Injector;
import lombok.RequiredArgsConstructor;
import noobbot.base.MessageType;
import noobbot.controller.PhysicsStore;
import noobbot.controller.Reactor;
import noobbot.data.CarControl;
import noobbot.data.CarInfo;
import noobbot.data.Cars;
import noobbot.data.SwitchLane;
import noobbot.msg.*;
import noobbot.physics.*;
import noobbot.wire.BotListener;
import noobbot.wire.BotWriter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anita on 4/17/14.
 */
@RequiredArgsConstructor
public class FixedBot implements BotListener {

    private final BotWriter botWriter;
    private boolean inQualifying;
    private static Cars allCarDetails;
    private GameInit gameData = null;
    private CarIdentity myCar;
    private static CarInfo carInfo = new CarInfo();
    private boolean inCrash;
    private List<TurboAvailable> turboData = new ArrayList<>();
    private double throttleFactor = 1.0;
    private double fixedThrottle = 0.66;

    private Injector injector;
    private Reactor reactor;

    public void init(Join join) {
        botWriter.send(MessageType.JOIN, join);
    }

    public void init(CreateRace race) {
        botWriter.send(MessageType.CREATE_RACE, race);
    }

    public void join(CreateRace race) {
        botWriter.send(MessageType.JOIN_RACE, race);
    }

    @Override
    public void handleYourCar(CarIdentity yourCar) {
        this.myCar = yourCar;
        carInfo.setMyCarIdentity(yourCar);
    }

    @Override
    public void handleGameInit(GameInit gameInit) {

        this.gameData = gameInit;
        this.inCrash = false;

        fillTrackDetails();

        carInfo = new CarInfo();
        carInfo.setMyCarIdentity(myCar);
        carInfo.setGameData(gameData);

        if (reactor != null) {
            reactor.shutdown();
        }

        ///                 Speed Model Training: A=0.198645, Fs=0.020359 (mse=0.101848)
        CurveVelocityModel2 curveVelocityModel = new CurveVelocityModel2();
        GamePhysics defaultPhysics = new GamePhysics(new GamePhysicsMap(new SpeedModel(
                new SpeedModel.ModelData(0.198645, 0.020359, 0.0)),
                new DriftModel(new DriftModel.ModelData(
                        0.048208,
                        0.012395,
                        0.002704,
                        0.011849))
        ), ImmutableMap.<Integer, GamePhysicsMap>of(),
                curveVelocityModel
        );

        allCarDetails = new Cars(gameData.getRace().getTrack().getPieces(), gameData.getRace().getCars().get(0).getDimensions(), defaultPhysics);
        allCarDetails.setMyCarIdentity(myCar);

        if (this.injector != null) {
            PhysicsStore physicsStore = this.injector.getInstance(PhysicsStore.class);
            defaultPhysics = physicsStore.get();
        }

        this.injector = Guice.createInjector(new ReactorModule(gameInit, allCarDetails, defaultPhysics));

        this.reactor = injector.getInstance(Reactor.class);
        if (this.reactor == null) {
            throw new IllegalStateException();
        }

        System.out.println("Brand New Reactor setup!");

    }

    private void fillTrackDetails() {

        Track track = this.gameData.getRace().getTrack();

        for (TrackPiece tp : track.getPieces()) {
            for (TrackLane lane : track.getLanes()) {

                // If it is a circular piece, calculate the actual length
                if (tp.getRadius() > 0) {

                    double actualRadius;

                    // Outer tracks are longer
                    if (tp.getAngle() > 0) {
                        actualRadius = tp.getRadius() - lane.getDistanceFromCenter();
                    } else {
                        actualRadius = tp.getRadius() + lane.getDistanceFromCenter();
                    }

                    tp.setComputedLaneRadius(lane.getIndex(), actualRadius);
                    tp.setComputedLaneLength(lane.getIndex(), 2 * Math.PI * actualRadius * Math.abs(tp.getAngle()) / 360);

                } else {
                    tp.setComputedLaneRadius(lane.getIndex(), 0);
                    tp.setComputedLaneLength(lane.getIndex(), tp.getLength());
                }
            }

            //System.out.printf("%.3f, %d, %.3f, %.3f, %.3f %n", tp.getLength(), tp.getRadius(), tp.getAngle(),
            //        tp.getComputedLaneLength(0), tp.getComputedLaneRadius(0));
        }

        // Find the longest sequence for straight pieces for turbo

        // Find the first curved piece
        int firstCurvedPiece = -1;
        int currentTrackIndex = 0;
        while (firstCurvedPiece == -1) {
            if (track.getPieces().get(currentTrackIndex).getAngle() > 0) {
                firstCurvedPiece = currentTrackIndex;
            }
            currentTrackIndex++;
        }

        int currentSeqBeginning = -1, prevSequence = -1, prevSequenceBeginning = -1;
        int currentSequence = 0;
        while (currentTrackIndex != firstCurvedPiece) {
            if (track.getPieces().get(currentTrackIndex).getAngle() == 0) {
                // Is this a new sequence
                if (currentSeqBeginning == -1) {
                    currentSeqBeginning = currentTrackIndex;
                }
                currentSequence += track.getPieces().get(currentTrackIndex).getLength();
            } else {
                // End of straights
                if (prevSequence <= currentSequence) {
                    prevSequenceBeginning = currentSeqBeginning;
                    prevSequence = currentSequence;
                }
                if (currentSeqBeginning > -1) {
                    // Update sequence length in track piece
                    int tempSequence = currentSeqBeginning;
                    while (tempSequence != currentTrackIndex) {
                        track.getPieces().get(tempSequence).setStraightLength(currentSequence);
                        currentSequence -= track.getPieces().get(tempSequence).getLength();
                        tempSequence++;
                        if (tempSequence == track.getPieces().size()) {
                            tempSequence = 0;
                        }
                    }
                }
                currentSeqBeginning = -1;
                currentSequence = 0;
            }
            currentTrackIndex++;
            if (currentTrackIndex == track.getPieces().size()) {
                currentTrackIndex = 0;
            }
        }

        if (prevSequence <= currentSequence) {
            prevSequenceBeginning = currentSeqBeginning;
            prevSequence = currentSequence;
        }

        track.getPieces().get(prevSequenceBeginning).setPotentialTurbo(true);
    }

    @Override
    public void handleGameStart(String gameId, int gameTick) {
        if (gameData.getRace().getRaceSession().getQuickRace() != null &&
                gameData.getRace().getRaceSession().getQuickRace()) {
            // No qualifying. Get running now
            inQualifying = false;
        } else {
            // If I am already in qualifying, I am now racing
            if (inQualifying) {
                inQualifying = false;
            } else {
                inQualifying = true;
            }
        }
        CarControl carControl = carInfo.getCarControl(reactor, false);
        executeCarControl(gameId, gameTick, carControl);
    }

    @Override
    public void handleCarPositions(CarPositions carPositions, String gameId, Integer gameTick) {
        boolean preStartPosition = false;
        if (gameTick == null) {
            gameTick = 0;
            preStartPosition = true;
        }

        CarPosition myCar = carPositions.findMatchingCar(carInfo.getMyCarIdentity());
        allCarDetails.updateCarPositions(carPositions, gameTick);

        if (reactor.isDataCollected() && allCarDetails.isCarBlocked()) {
            reactor.stopDataCollection();
        }

        if (!reactor.isDataCollected() && !allCarDetails.isCarBlocked()) {
            reactor.startDataCollection();
        }


        if (inCrash) {
            System.out.println("In Crash.");
            sendPing();
        } else {
            carInfo.updatePosition(myCar, gameTick);
            CarControl carControl = carInfo.getCarControl(reactor, preStartPosition);
            if (preStartPosition) {
                System.out.println("Not going to send any response now!");
                sendPing();
            } else {
                executeCarControl(gameId, gameTick, carControl);
            }
            carInfo.log();
            carInfo.finishUpdate();
        }

    }

    private void executeCarControl(String gameId, Integer gameTick, CarControl carControl) {
        botWriter.throttle(fixedThrottle, gameId, gameTick);
    }

    @Override
    public void handleGameEnd(GameEnd gameEnd) {
        reactor.shutdown();
        reactor = null;
    }

    @Override
    public void sendPing() {
        botWriter.send(MessageType.PING, null);
    }

    @Override
    public void handleCrash(CarIdentity data, String gameId, Integer gameTick) {
        allCarDetails.carCrashed(data);


        if (data.getName().equals(carInfo.getMyCarIdentity().getName())) {
            sendPing();
            carInfo.updateCrash();
            reactor.updateCrash();
            inCrash = true;
        }
    }

    @Override
    public void handleSpawn(CarIdentity data, String gameId, Integer gameTick) {
        allCarDetails.carSpawned(data);
        if (data.getName().equals(carInfo.getMyCarIdentity().getName())) {
            inCrash = false;
            reactor.updateSpawn();
        }
    }

    @Override
    public void handleLapFinished(LapFinished data) {
    }

    @Override
    public void shutdown() {
        System.exit(0);
    }

    @Override
    public void handleDnf(CarIdentity data) {
    }

    @Override
    public void handleTournamentEnd() {
        shutdown();
    }

    @Override
    public void handleFinish(CarIdentity data) {

    }

    @Override
    public void sendJoinRace(CreateRace race) {
        botWriter.send(MessageType.JOIN_RACE, race);
    }

    @Override
    public void handleTurboAvailable(TurboAvailable turboData) {
        allCarDetails.turboAvailable(turboData);
        this.turboData.add(turboData);
    }

    @Override
    public void handleTurboStart(CarIdentity car, Integer gameTick) {
        allCarDetails.turboStarted(car);
        carInfo.setTurboApplied(this.turboData.get(0));
        carInfo.setTurboStartTime(gameTick);
        // TODO(anita): make it queue?
        this.turboData.remove(0);
    }

    @Override
    public void handleTurboEnd(CarIdentity car, Integer gameTick) {
        carInfo.setTurboApplied(null);
        allCarDetails.turboEnd(car);
    }

    private void turboBoost() {
        botWriter.send(MessageType.TURBO, "Whee!!!");
    }

    public void setThrottleFactor(double factor) {
        System.out.printf("Throttle Factor : %.3f%n", factor);
        this.throttleFactor = factor;
    }
}
