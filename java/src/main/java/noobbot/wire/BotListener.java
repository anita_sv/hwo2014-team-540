package noobbot.wire;

import noobbot.msg.*;

/**
 * Created by anita on 4/17/14.
 */
public interface BotListener {

    void handleYourCar(CarIdentity yourCar);

    void handleGameInit(GameInit gameInit);

    void handleGameStart(String gameId, int gameTick);

    void handleCarPositions(CarPositions carPositions,
                                   String gameId,
                                   Integer gameTick);

    void handleGameEnd(GameEnd gameEnd);

    void sendPing();

    void handleCrash(CarIdentity car, String gameId, Integer gameTick);

    void handleSpawn(CarIdentity car, String gameId, Integer gameTick);

    void handleLapFinished(LapFinished data);

    void shutdown();

    void handleDnf(CarIdentity car);

    void handleTournamentEnd();

    void handleFinish(CarIdentity car);

    void sendJoinRace(CreateRace race);

    void handleTurboAvailable(TurboAvailable turboData);

    void handleTurboStart(CarIdentity car, Integer gameTick);

    void handleTurboEnd(CarIdentity car, Integer gameTick);
}
