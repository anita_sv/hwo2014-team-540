package noobbot.wire;

import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import noobbot.base.MsgWrapper;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Created by anita on 4/17/14.
 */
@RequiredArgsConstructor
public class BotReader {

    private final Gson gson;

    private final BufferedReader bufferedReader;

    public MsgWrapper next()
            throws IOException {
        String line = bufferedReader.readLine();
        System.out.println("RECV: " + line);
        return gson.fromJson(line, MsgWrapper.class);
    }
}
