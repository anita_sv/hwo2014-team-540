package noobbot.wire;

import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import noobbot.base.MessageType;
import noobbot.base.MsgWrapper;

import java.io.PrintWriter;

/**
 * Created by anita on 4/17/14.
 */
@RequiredArgsConstructor
public class BotWriter {

    private final Gson gson;

    private final PrintWriter printWriter;

    private void writeInternal(MsgWrapper object) {
        String json = gson.toJson(object);
        System.out.println("SEND " + json);
        printWriter.println(json);
        printWriter.flush();
    }

    public void send(MessageType type, Object data) {
        // Race doesn't have ticks and gameId
        try {
            if (data != null && !type.getClazz().isInstance(data)) {
                throw new IllegalArgumentException("Data != null, but type must match");
            }
            MsgWrapper msgWrapper = new MsgWrapper(type, data,
                    null, null);
            writeInternal(msgWrapper);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public void throttle(double throttle, String gameId, Integer gameTick) {
        MsgWrapper msgWrapper = new MsgWrapper(MessageType.THROTTLE, throttle,
                gameId, gameTick);
        writeInternal(msgWrapper);
    }

}
