package noobbot.wire;

import lombok.RequiredArgsConstructor;
import noobbot.base.MsgWrapper;
import noobbot.msg.*;

import java.io.IOException;

/**
 * Created by anita on 4/17/14.
 */
@RequiredArgsConstructor
public class BotEngine {

    private final BotListener botListener;

    private final BotReader botReader;

    public void loop() throws IOException {
        while (true) {

            MsgWrapper msgWrapper = botReader.next();
            if (msgWrapper == null) {
                return;
            }
            switch (msgWrapper.getMsgType()) {

                case YOUR_CAR:
                    botListener.handleYourCar((CarIdentity) msgWrapper.getData());
                    break;

                case GAME_INIT:
                    botListener.handleGameInit((GameInit) msgWrapper.getData());
                    break;

                case GAME_START:
                    botListener.handleGameStart(msgWrapper.getGameId(), msgWrapper.getGameTick());
                    break;

                case CAR_POSITIONS:
                    botListener.handleCarPositions((CarPositions) msgWrapper.getData(),
                            msgWrapper.getGameId(), msgWrapper.getGameTick());
                    break;

                case GAME_END:
                    botListener.handleGameEnd((GameEnd) msgWrapper.getData());
                    break;

                case CRASH:
                    botListener.handleCrash((CarIdentity) msgWrapper.getData(), msgWrapper.getGameId(),
                            msgWrapper.getGameTick());
                    break;
                case SPAWN:
                    botListener.handleSpawn((CarIdentity) msgWrapper.getData(), msgWrapper.getGameId(),
                            msgWrapper.getGameTick());
                    break;

                case LAP_FINISHED:
                    botListener.handleLapFinished((LapFinished) msgWrapper.getData());
                    break;

                case FINISH:
                    botListener.handleFinish((CarIdentity) msgWrapper.getData());
                    break;

                case TOURNAMENT_END:
                    botListener.handleTournamentEnd();
                    break;

                case DNF:
                    botListener.handleDnf((CarIdentity) msgWrapper.getData());
                    break;

                default:
                    botListener.sendPing();
                    break;

                case JOIN:
                    botListener.sendPing();
                    break;

                case THROTTLE:
                    botListener.sendPing();
                    break;

                case PING:
                    botListener.sendPing();
                    break;
                case UNKNOWN:
                    System.out.println("Unknown message. Data : " + msgWrapper.getData().toString());
                    break;

                case CREATE_RACE:
                    // botListener.sendJoinRace((CreateRace)msgWrapper.getData());
                    break;

                case SWITCH_LANE:
                    botListener.sendPing();
                    break;

                case TURBO_AVAILABLE:
                    botListener.handleTurboAvailable((TurboAvailable) msgWrapper.getData());
                    break;

                case TURBO_START:
                    botListener.handleTurboStart((CarIdentity)msgWrapper.getData(), msgWrapper.getGameTick());
                    break;

                case TURBO_END:
                    botListener.handleTurboEnd((CarIdentity)msgWrapper.getData(), msgWrapper.getGameTick());
                    break;

                case ERROR:
                    System.out.println(((String) msgWrapper.getData()));
                    return;
            }
        }
    }
}
