package noobbot.physics;

import lombok.Getter;
import lombok.ToString;
import lombok.experimental.Builder;
import noobbot.data.FullMetric;
import noobbot.data.SwitchLane;
import noobbot.msg.PiecePosition;

/**
* Created by anita on 4/23/14.
*/
@Getter
@Builder @ToString
public class SolvingParams {
    private final PiecePosition piecePosition;
    private final FullMetric fullMetric;
    private final int gameTick;
    private final SwitchLane firedNext;
}
