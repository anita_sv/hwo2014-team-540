package noobbot.physics;

import noobbot.math.CoarseDerivative;
import noobbot.math.Derivative;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by anita on 4/20/14.
 */
public class AnalyseDrift {
    public static void main(String[] args) throws IOException {
        File file = new File(args[0]);
        if (!file.exists() || !file.canRead()) {
            return;
        }
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        String line;

        List<Double> distanceList = new ArrayList<>();
        List<Double> curvatureList = new ArrayList<>();
        List<Double> driftList = new ArrayList<>();

        while ((line = bufferedReader.readLine()) != null) {
            String[] splits = line.split(",");
//            Integer tick = Integer.parseInt(splits[0]);
            Double distance = Double.parseDouble(splits[1]);
            Double direction = Double.parseDouble(splits[2]);
            Double curvature = Double.parseDouble(splits[3]);
            Double drift = Double.parseDouble(splits[4]);

//            while (nextTick <= tick) {
                distanceList.add(distance);
                curvatureList.add(curvature * direction);
                driftList.add(drift);
//                nextTick++;
//            }
        }

        Derivative derivative = new CoarseDerivative();
        List<Double> velocityList = derivative.derive(distanceList);
        List<Double> accelerationList = derivative.derive(velocityList);

        List<Double> driftRateList = derivative.derive(driftList);
        List<Double> driftAccelList = derivative.derive(driftRateList);

        List<Double> cosDrift = cos(driftList);
        List<Double> sinDrift = sin(driftList);

        List<Double> radialForceList = multiply(multiply(multiply(velocityList, velocityList), curvatureList),
                cosDrift);

        List<Double> axialForceList = multiply(accelerationList, sinDrift);

        List<Double> netForceList = subtract(radialForceList, axialForceList);

        List<Double> directionalList = multiply(velocityList, driftRateList);

        List<Double> velocitySquareList = multiply(velocityList, velocityList);

        writeToFile(netForceList, args[1] + "_net_force.tsv");
        writeToFile(driftAccelList, args[1] + "_accel.tsv");
        writeToFile(directionalList, args[1] + "_directional.tsv");
        writeToFile(driftList, args[1] + "_drift.tsv");
        writeToFile(driftRateList, args[1] + "_drift_rate.tsv");
        writeToFile(radialForceList, args[1] + "_radial_force.tsv");
        writeToFile(axialForceList, args[1] + "_axial_force.tsv");
        writeToFile(velocitySquareList, args[1] + "_velocity_sq.tsv");

        Universe universe = new Universe();

        String[] names = {
                "A",
                "B",
                "C"
        };

        List<ForceCoeff> terms = universe.solveDrift(driftList, driftRateList, driftAccelList, velocityList);

        System.out.println("Solved!");

        for (int c = 0; c < names.length; c++) {
            File output = new File(args[1] + "_" + names[c] + ".tsv");
            PrintWriter printWriter = new PrintWriter(output);
            for (ForceCoeff forceCoeff : terms) {
                printWriter.println(forceCoeff.getTime() + "\t" +
                        forceCoeff.getForces().get(c));
            }
            printWriter.flush();
            printWriter.close();
        }
    }

    private static void writeToFile(List<Double> outputList, String fileName) throws FileNotFoundException {
        File outputFile = new File(fileName);
        PrintWriter outputWriter = new PrintWriter(outputFile);
        for (int i = 0; i < outputList.size(); i++) {
            outputWriter.println(i + "\t" + outputList.get(i));
        }
        outputWriter.close();
    }

    private static List<Double> subtract(List<Double> a, List<Double> b) {
        List<Double> product = new ArrayList<>(a.size());
        for (int i = 0; i < a.size(); i++) {
            product.add(a.get(i) - b.get(i));
        }
        return product;
    }

    private static List<Double> cos(List<Double> driftList) {
        List<Double> product = new ArrayList<>(driftList.size());
        for (int i = 0; i < driftList.size(); i++) {
            product.add(Math.cos(driftList.get(i) * Math.PI / 180));
        }
        return product;
    }

    private static List<Double> sin(List<Double> driftList) {
        List<Double> product = new ArrayList<>(driftList.size());
        for (int i = 0; i < driftList.size(); i++) {
            product.add(Math.sin(driftList.get(i) * Math.PI / 180));
        }
        return product;
    }

    private static List<Double> multiply(List<Double> a, List<Double> b) {
        List<Double> product = new ArrayList<>(a.size());
        for (int i = 0; i < a.size(); i++) {
            product.add(a.get(i) * b.get(i));
        }
        return product;
    }

}
