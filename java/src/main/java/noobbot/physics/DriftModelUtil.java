package noobbot.physics;

import noobbot.data.FullMetric;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anita on 4/22/14.
 */
public class DriftModelUtil {

    private DriftModelUtil() {}

    public static DriftModel.Input makeInput(FullMetric fullMetric) {

//        double futureAcceleration =
//                speedModel.findAcceleration(new SpeedModel.Input(fullMetric.getThrottleMetric().getThrottle(),
//                fullMetric.getPositionMetric().getVelocity()));
//        double futureVelocity = fullMetric.getPositionMetric().getVelocity() + futureAcceleration;

        double acceleration = fullMetric.getPositionMetric().getAcceleration();
        double velocity = fullMetric.getPositionMetric().getVelocity();

        return DriftModel.Input.builder()
                .setAcceleration(acceleration)
                .setCurvature(fullMetric.getSignedCurvature())
                .setDriftDegrees(fullMetric.getDriftMetric().getDriftDegrees())
                .setDriftDegreesRate(fullMetric.getDriftMetric().getDriftRateDegrees())
                .setVelocity(velocity)
                .build();
    }

    public static List<DriftModel.Input> prepareInputList(List<FullMetric> fullMetricList) {
        List<DriftModel.Input> driftModelInputList = new ArrayList<>();
        // Skip one entry because that is not useful because we don't know the corresponding driftAccel.
        for (int i = 0; i + 1 < fullMetricList.size(); i++) {
            driftModelInputList.add(makeInput(fullMetricList.get(i)));
        }
        return driftModelInputList;
    }

    public static List<DriftModel.Output> prepareOutputList(List<FullMetric> fullMetricList) {
        List<DriftModel.Output> driftModelOutputList = new ArrayList<>();
        for (int i = 0; i + 1 < fullMetricList.size(); i++) {
            // Get the next one because it is predicting the next value.
            driftModelOutputList.add(new DriftModel.Output(fullMetricList.get(i + 1)
                    .getDriftMetric().getDriftAccelDegrees()));
        }
        return driftModelOutputList;
    }

    public static DriftModel train(List<FullMetric> fullMetricList) {
        List<FullMetric> screenedFullMetric = new ArrayList<>();
        for (FullMetric fullMetric : fullMetricList) {
            if (Math.abs(fullMetric.getDriftMetric().getDriftDegrees()) > 5) {
                screenedFullMetric.add(fullMetric);
            }
        }
        return new DriftModel(DriftModel.train(prepareInputList(screenedFullMetric),
                prepareOutputList(screenedFullMetric)));
    }
}
