package noobbot.physics;

import edu.wlu.cs.levy.CG.KDTree;
import lombok.Data;
import noobbot.msg.GameInit;
import noobbot.msg.TrackPiece;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by vaidhy.gopalan on 11/05/14.
 */
public class CurveVelocityModel {

    private KDTree<CurveValue> actualMap;
    private KDTree<CurveValue> estimateMap;

    private double largeFactor = 0.5, mediumFactor = 0.4, smallFactor = 0.3;
    private double defaultThrottle = 0.5;
    private double DRIFT_EPSILON = 3;
    private double MAX_DRIFT = 60;
    private List<TrackPiece> trackPieces;
    private Map<Integer, Hints> trackHints;

    @Data
    public static class CurveValue {
        private double curvature;
        private double length;
        private double entryDrift;
        private double entryDriftRate;

        private boolean crashed;
        private double velocity;
        private double maxDrift;
    }

    @Data
    public static class Hints {
        private boolean followedByCurve;
        private boolean precededByCurve;
        private boolean driftTooHigh;
    }


    public CurveVelocityModel() {
        this.actualMap = new KDTree<CurveValue>(4);
        this.estimateMap = new KDTree<CurveValue>(3);
        this.trackHints = new HashMap<Integer, Hints>();
    }

    public void setTrackPieces(List<TrackPiece> trackPieces) {
        this.trackPieces = trackPieces;
        int index = 0;

        TrackPiece piece;
        while (index < trackPieces.size() - 1) {
            piece = trackPieces.get(index);

            if (piece.getRadius() > 0) {
                for (int i = 0; i < piece.getComputedCurvature().size(); i++) {
                    System.out.println("Init - " + index + " " + piece.getComputedCurvature(i) + " " + piece.getComputedLaneLength(i));
                    initPiece(index, piece.getComputedCurvature(i), piece.getComputedLaneLength(i));
                }
            }

            if ((piece.getRadius() > 0) &&
                    piece.getAngleRemaining() == piece.getAngle()) {
                if (trackPieces.get(index + 1).getRadius() > 0) {
                    Hints h = new Hints();
                    h.setFollowedByCurve(true);
                    trackHints.put(index, h);

                    h = new Hints();
                    h.setPrecededByCurve(true);
                    trackHints.put(index+1, h);
                }
            }
            index++;
        }

        piece = trackPieces.get(index);

        if (piece.getRadius() > 0) {
            for (int i = 0; i < piece.getComputedCurvature().size(); i++) {
                System.out.println("Init - " + index + " " + piece.getComputedCurvature(i) + " " + piece.getComputedLaneLength(i));
                initPiece(index, piece.getComputedCurvature(i), piece.getComputedLaneLength(i));
            }
        }

    }

    private void initPiece(int pieceIndex, double signedCurvature, double length) {
        if (signedCurvature < 0) {
            signedCurvature *= -1;
        }
        
        System.out.printf("Init Piece - Index %d, Curvature : %.3f, Length: %.3f%n", pieceIndex, signedCurvature, length);

        CurveValue cv = new CurveValue();
        cv.setCurvature(signedCurvature);
        cv.setLength(length);
        cv.setVelocity(getInitialVelocity(signedCurvature));
        try {
            double radius = 1/signedCurvature;
            estimateMap.insert(new double[]{pieceIndex, radius, length}, cv);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void updatePieceAtStart(int pieceIndex, double signedCurvature, double length, double entryDrift, double entryDriftRate ) {
        if (signedCurvature < 0) {
            signedCurvature *= -1;
            entryDrift *= -1;
            entryDriftRate *= -1;
        }

        CurveValue cv = getExactCV(pieceIndex, signedCurvature, length);
        cv.setEntryDrift(entryDrift);
        cv.setEntryDriftRate(entryDriftRate);

    }
    public void updatePieceAtEnd(int pieceIndex, double signedCurvature, double length, double maxDrift) {
        if (signedCurvature < 0) {
            signedCurvature *= -1;
            maxDrift *= -1;
        }

        CurveValue cv = getExactCV(pieceIndex, signedCurvature, length);
        cv.setMaxDrift(maxDrift);
        cv.setVelocity(adjustVelocity(cv.getVelocity(), cv.getMaxDrift()));
        updateActuals(pieceIndex, signedCurvature, length, cv.getEntryDrift(), cv.getEntryDriftRate(), maxDrift);
        cv.setVelocity(adjustForHints(pieceIndex, computeNewEstimate(pieceIndex, signedCurvature, length)));
    }

    public void updateCrash(int pieceIndex, double signedCurvature, double length, double inPieceDistance) {
        if (signedCurvature < 0) {
            signedCurvature *= -1;
        }

        CurveValue cv = getExactCV(pieceIndex, signedCurvature, length);
        cv.setCrashed(true);
        cv.setVelocity(updateCrashVelocity(pieceIndex, cv.getVelocity(), length, inPieceDistance));
    }

    public double getVelocity(int pieceIndex, double signedCurvature, double length) {
        if (signedCurvature < 0) {
            signedCurvature *= -1;
        }

        CurveValue cv = getExactCV(pieceIndex, signedCurvature, length);
        return cv.getVelocity();
    }

    public double getVelocity(int pieceIndex, double signedCurvature, double length,  double entryDrift, double entryDriftRate) {
        if (signedCurvature < 0) {
            signedCurvature *= -1;
            entryDrift *= -1;
            entryDriftRate *= -1;
        }

        CurveValue cv = getExactCV(pieceIndex, signedCurvature, length);
        return cv.getVelocity();

        // Have we seen this before
//        try {
//            cv = curveHistory.nearest(new double[]{signedCurvature, entryDrift, entryDriftRate});
//            if (cv.getCurvature() == signedCurvature) {
//                // OK.. I have seen something of the same curvature
//                double driftDifference = entryDrift - cv.getEntryDrift();
//                if (Math.abs(driftDifference) < DRIFT_EPSILON) {
//                    double newVelocity = adjustVelocity(cv.getVelocity(), cv.getMaxDrift());
//                    CurveValue newCV = new CurveValue();
//                    newCV.setCurvature(signedCurvature);
//                    newCV.setEntryDrift(entryDrift);
//                    newCV.setEntryDriftRate(entryDriftRate);
//                    newCV.setVelocity(newVelocity);
//                    curveHistory.insert(new double[]{signedCurvature, entryDrift, entryDriftRate}, cv);
//                    return newVelocity;
//                } else {
//                    // I have seen the same curvature, but the new drift is very high
//                }
//            }
//
//            // I do not have anything of the same curvature
//
//
//
//        } catch (Exception ex) {
//            return defaultThrottle;
//        }
//        return defaultThrottle;
    }

    private double adjustForHints(int pieceIndex, double velocity) {
        if (trackHints.containsKey(pieceIndex)) {
            Hints h = trackHints.get(pieceIndex);
            if (h.isFollowedByCurve()) {
                velocity /= 2;
            } else if (h.isPrecededByCurve()) {
                velocity /= 2;
            } else if (h.driftTooHigh) {
                velocity *= 0.75;
            }
        }
        return velocity;
    }

    private double adjustVelocity(double oldVelocity, double maxDrift) {
        // TODO - Get this mapping right
        double exponent =  (maxDrift/MAX_DRIFT * 2) - 1;
        double newFactor = 1 - (1.25/(1 + Math.exp(-2 * exponent)));
        return oldVelocity + 1 * newFactor;
    }

    private double getInitialVelocity(double signedCurvature) {
        if (signedCurvature >= 0.01) {
            return 3.0;
        }
        if (signedCurvature <= 0.005) {
            return 7.0;
        }
        return 5.5;
    }

    private double updateCrashVelocity(int pieceIndex, double velocity, double totalPieceLength, double inPieceDistance) {
        double inPieceRatio = inPieceDistance/totalPieceLength;
        return velocity * 0.8;
    }

//    private void updatePreviousPieceVelocity(int previousPieceIndex) {
//        CurveValue cv = pieceMap.get(previousPieceIndex);
//        cv.setVelocity(cv.getVelocity() * 0.8);
//
//    }

    private CurveValue getExactCV(int pieceIndex, double curvature, double length) {
        CurveValue cv = null;
        
        double radius = 1/curvature;

        try {
            cv = estimateMap.search(new double[]{pieceIndex, radius, length});
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return cv;
    }

    private void updateActuals(int pieceIndex, double curvature, double length, double entryDrift, double entryDriftRate, double maxDrift) {
        CurveValue cv = getExactCV(pieceIndex, curvature, length);
        if (cv != null) {
            double velocity = cv.getVelocity();

            try {
                double radius = 1/curvature;
                CurveValue ca = actualMap.search(new double[]{radius, length, entryDrift, entryDriftRate});
                if (ca == null) {
                    ca = new CurveValue();
                    ca.setCurvature(curvature);
                    ca.setLength(length);
                    ca.setEntryDrift(entryDrift);
                    ca.setEntryDriftRate(entryDriftRate);
                    actualMap.insert(new double[]{radius, length, entryDrift, entryDriftRate}, ca);
                }
                ca.setVelocity(velocity);
                ca.setMaxDrift(maxDrift);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
    
    private double computeNewEstimate(int pieceIndex, double curvature, double length) {
        // Get the piece from estimate for entry drift and entry drift rate
        CurveValue cv = getExactCV(pieceIndex, curvature, length);
        double entryDrift = 0.0, entryDriftRate = 0.0;
        if (cv != null) {
            entryDrift = cv.getEntryDrift();
            entryDriftRate = cv.getEntryDriftRate();
        }

        return computeNewEstimate(pieceIndex, curvature, length, entryDrift, entryDriftRate);
    }

    private double computeNewEstimate(int pieceIndex, double curvature, double length, double entryDrift, double entryDriftRate) {
        // Get stuff from actual history
        try {
            double radius = 1/curvature;
            List<CurveValue> values = actualMap.nearest(new double[]{radius, length, entryDrift, entryDriftRate}, 10);
            // Let us try to interpolate

            double exponent = 0, newFactor = 1;
            //Find others with same curvature and length
            List<CurveValue> tempList = new ArrayList<CurveValue>();
            for (CurveValue v : values) {
                if ((v.getCurvature() == curvature) && (v.getLength() == length)) {
                    tempList.add(v);
                }
            }

            double sum_velocities = 0.0;

            if (tempList.size() > 0) {
                // OK We got something that matches. We will only use this to extrapolate
                for (CurveValue v : tempList) {
                    double d = v.getMaxDrift() - v.getEntryDrift() + entryDrift;

                    if (d > MAX_DRIFT) {
                        exponent =  (MAX_DRIFT/d * 2) - 1;
                        newFactor =  (-1/(1 + Math.exp(-2 * exponent)));

                    } else {
                        exponent =  (d/MAX_DRIFT * 2) - 1;
                        newFactor = 1 - (1.25/(1 + Math.exp(-2 * exponent)));

                    }
                    sum_velocities =  v.getVelocity() + newFactor;
                }

                return sum_velocities/tempList.size();
            }

            // OK.. nothing matches our curvature and length. Let us try to find one with higher curvature and reduce down
            sum_velocities = 0.0;

            if (tempList.size() > 0) {
                for (CurveValue v : values) {
                    sum_velocities =  v.getVelocity() * v.getCurvature()/curvature * 0.8;
                }

                return sum_velocities/tempList.size();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return getInitialVelocity(curvature);
    }
}
