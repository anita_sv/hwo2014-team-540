package noobbot.physics;

import lombok.Data;

import java.util.Map;

/**
 * Created by vaidhy on 21/4/14.
 */

@Data
public class GamePhysics {

    private final GamePhysicsMap defaultPhysics;

    private final Map<Integer, GamePhysicsMap> gamePhysicsAtlas;

    private final CurveVelocityModel2 curveVelocityModel;

    public GamePhysicsMap getGamePhysicsMap(int index) {
        GamePhysicsMap gamePhysicsMap =  gamePhysicsAtlas.get(index);
        if (gamePhysicsMap == null) {
            return defaultPhysics;
        }
        return gamePhysicsMap;
    }

}
