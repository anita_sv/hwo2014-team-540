package noobbot.physics;

import noobbot.math.CoarseDerivative;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 */
public class RegressionLinear {

    public static void main(String[] args) throws IOException {

        File file = new File(args[0]);
        if (!file.exists() || !file.canRead()) {
            return;
        }
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        String line;
        int nextTick = 0;
        List<Double> positionList = new ArrayList<>();
        List<Double> throttleList = new ArrayList<>();

        while ((line = bufferedReader.readLine()) != null) {
            String[] splits = line.split(",");
            Integer tick = Integer.parseInt(splits[0]);
            Double position = Double.parseDouble(splits[1]);
            Double throttle = Double.parseDouble(splits[2]);
            while (nextTick <= tick) {
                positionList.add(position);
                throttleList.add(throttle);
                nextTick++;
            }
        }

        List<Double> velocityList = (new CoarseDerivative()).derive(positionList);
        List<Double> accelerationList = (new CoarseDerivative()).derive(velocityList);

        List<SpeedModel.Input> inputList = new ArrayList<>();
        List<SpeedModel.Output> outputList = new ArrayList<>();

        for (int i = 1; i + 1< velocityList.size(); i++) {
            double velocity = velocityList.get(i);
            double throttle = throttleList.get(i);
            double acceleration = accelerationList.get(i + 1);
            SpeedModel.Input input = new SpeedModel.Input(throttle, velocity);
            SpeedModel.Output output = new SpeedModel.Output(acceleration);

            inputList.add(input);
            outputList.add(output);
        }

        SpeedModel.ModelData modelData = SpeedModel.train(inputList, outputList);

        System.out.println("A = " + modelData.getMaxAcceleration());
        System.out.println("Fs = " + modelData.getCoefficientOfViscosity());
        System.out.println("Max Speed = " +
                modelData.getMaxAcceleration() / modelData.getCoefficientOfViscosity());
    }
}
