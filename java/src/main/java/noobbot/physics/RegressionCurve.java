package noobbot.physics;

import noobbot.math.Derivative;
import noobbot.math.TwoPointDerivative;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by anita on 4/21/14.
 */
public class RegressionCurve {
    public static void main(String[] args) throws IOException {

        String inputFileName = args[0];

        File file = new File(inputFileName);

        if (!file.exists() || !file.canRead()) {
            System.out.println("File not readable");
            return;
        }
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        String line;
        int nextTick = 0;

        List<Double> distanceList = new ArrayList<>();
        List<Double> curvatureList = new ArrayList<>();
        List<Double> driftList = new ArrayList<>();

        while ((line = bufferedReader.readLine()) != null) {
            String[] splits = line.split(",");
            Integer tick = Integer.parseInt(splits[0]);
            Double distance = Double.parseDouble(splits[1]);
            Double direction = Double.parseDouble(splits[2]);
            Double curvature = Double.parseDouble(splits[3]);
            Double drift = Double.parseDouble(splits[4]);

            while (nextTick <= tick) {
                distanceList.add(distance);
                curvatureList.add(direction * curvature);
                driftList.add(drift);
                nextTick++;
            }
        }

        Derivative derivative = new TwoPointDerivative();
        List<Double> velocityList = derivative.derive(distanceList);
        List<Double> accelerationList = derivative.derive(velocityList);

        List<Double> driftRateList = derivative.derive(driftList);
        List<Double> driftAccelList = derivative.derive(driftRateList);

        List<DriftModel.Input> driftInputList = new ArrayList<>();
        List<DriftModel.Output> driftOutputList = new ArrayList<>();

        for (int i = 0; i + 1 < velocityList.size(); i++) {

            if (Math.abs(driftList.get(i)) > 1) {
                continue;
            }
            DriftModel.Input input = DriftModel.Input.builder()
                    .setAcceleration(accelerationList.get(i))
                    .setCurvature(curvatureList.get(i))
                    .setDriftDegrees(driftList.get(i))
                    .setDriftDegreesRate(driftRateList.get(i))
                    .setVelocity(velocityList.get(i))
                    .build();

            DriftModel.Output output = new DriftModel.Output(
                    driftAccelList.get(i + 1));

            driftInputList.add(input);
            driftOutputList.add(output);
        }

        DriftModel.ModelData driftModelData = DriftModel.train(driftInputList, driftOutputList);



        System.out.println(driftModelData);

    }

    private static List<Double> subtract(List<Double> a, List<Double> b) {
        List<Double> product = new ArrayList<>(a.size());
        for (int i = 0; i < a.size(); i++) {
            product.add(a.get(i) - b.get(i));
        }
        return product;
    }

    private static List<Double> cos(List<Double> driftList) {
        List<Double> product = new ArrayList<>(driftList.size());
        for (int i = 0; i < driftList.size(); i++) {
            product.add(Math.cos(driftList.get(i) * Math.PI / 180));
        }
        return product;
    }

    private static List<Double> sin(List<Double> driftList) {
        List<Double> product = new ArrayList<>(driftList.size());
        for (int i = 0; i < driftList.size(); i++) {
            product.add(Math.sin(driftList.get(i) * Math.PI / 180));
        }
        return product;
    }

    private static List<Double> multiply(List<Double> a, List<Double> b) {
        List<Double> product = new ArrayList<>(a.size());
        for (int i = 0; i < a.size(); i++) {
            product.add(a.get(i) * b.get(i));
        }
        return product;
    }

}

