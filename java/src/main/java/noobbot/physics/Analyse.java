package noobbot.physics;

import noobbot.math.CoarseDerivative;
import noobbot.math.TwoPointDerivative;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by anita on 4/19/14.
 */
public class Analyse {

    public static void main(String[] args) throws IOException {
        File file = new File(args[0]);
        if (!file.exists() || !file.canRead()) {
            return;
        }
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        String line;
        int nextTick = 0;
        List<Double> positionList = new ArrayList<>();
        double nextPosition = 0;
        while ((line = bufferedReader.readLine()) != null) {
            String[] splits = line.split(",");
            Integer tick = Integer.parseInt(splits[0]);
            Double position = Double.parseDouble(splits[1]);
            while (nextTick <= tick) {
                positionList.add(position);
                nextTick++;
            }
        }

        List<Double> velocityList = (new CoarseDerivative()).derive(positionList);
        List<Double> accelerationList = (new TwoPointDerivative()).derive(velocityList);

        File velocityFile = new File(args[1] + "_velocity.txt");
        PrintWriter velocityWriter = new PrintWriter(velocityFile);
        for (Double velocity : velocityList) {
            velocityWriter.println(velocity);
        }
        velocityWriter.flush();
        velocityWriter.close();

        File accelFile = new File(args[1] + "_accel.txt");
        PrintWriter accelWriter = new PrintWriter(accelFile);
        for (Double accel : accelerationList) {
            accelWriter.println(accel);
        }
        accelWriter.flush();
        accelWriter.close();

        Universe universe = new Universe();


        String[] names = {
                "A",
                "fs",
        };

        int dim = names.length;
        List<ForceCoeff> force = universe.solve(velocityList, accelerationList, dim);

        for (int c = 0; c < dim; c++) {
            File output = new File(args[1] + "_" + dim + "_" + names[c] + ".txt");
            PrintWriter printWriter = new PrintWriter(output);
            for (ForceCoeff forceCoeff : force) {
                printWriter.println(forceCoeff.getTime() + "\t" +
                        forceCoeff.getForces().get(c));
            }
            printWriter.flush();
            printWriter.close();
        }
    }
}
