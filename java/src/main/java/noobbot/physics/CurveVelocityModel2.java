package noobbot.physics;

import lombok.Data;
import noobbot.msg.TrackPiece;
import sun.awt.geom.Curve;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by vaidhy.gopalan on 12/05/14.
 */
public class CurveVelocityModel2 {
    private List<TrackPiece> trackPieces;
    private double MAX_DRIFT = 45;

    @Data
    public static class Hints {
        private boolean followedByCurve;
        private boolean precededByCurve;
        private boolean driftTooHigh;
    }

    @Data
    public static class CurveValue {
        private double velocity;
        private double maxDrift;
        private boolean crashed;
    }

    private Map<Integer, Double> lastPieceVelocity;


    private Map<Integer, Map<Integer, CurveValue>> velocityEstimates;
    private Map<Integer, Hints> trackHints;

    public CurveVelocityModel2() {
        this.velocityEstimates = new HashMap<>();
        this.trackHints = new HashMap<>();
        this.lastPieceVelocity = new HashMap<>();
    }

    public void setTrackPieces(List<TrackPiece> trackPieces) {
        this.trackPieces = trackPieces;
        int index = 0;

        while (index < trackPieces.size() - 1) {
            TrackPiece piece = trackPieces.get(index);
            if (piece.getRadius() > 0) {
                this.lastPieceVelocity.put(index, getInitialVelocity(piece.getComputedCurvature(0) * piece.getDirection()));

                if (piece.getAngleRemaining() == piece.getAngle()) {
                    if ((trackPieces.get(index + 1).getRadius() > 0) &&
                            (trackPieces.get(index + 1).getRadius() < 60)) {
                        Hints h = new Hints();
                        h.setFollowedByCurve(true);
                        trackHints.put(index, h);

                        h = new Hints();
                        h.setPrecededByCurve(true);
                        trackHints.put(index + 1, h);
                    }
                }
            }
            index++;
        }
    }

    public void updatePieceAtEnd(int pieceIndex, int lane, double maxDrift) {
        maxDrift = Math.abs(maxDrift);
        Map<Integer, CurveValue> ev;
        if (velocityEstimates.containsKey(pieceIndex)) {
            ev = velocityEstimates.get(pieceIndex);
        } else {
            ev = new HashMap<>();
            velocityEstimates.put(pieceIndex, ev);
        }

        CurveValue cv;
        if (ev.containsKey(lane)) {
            cv = ev.get(lane);
        } else {
            cv = new CurveValue();
            cv.setVelocity(lastPieceVelocity.get(pieceIndex));
            ev.put(lane, cv);
        }

        cv.setMaxDrift(maxDrift);
        if (!cv.isCrashed()) {
            cv.setVelocity(recomputeVelocity(pieceIndex, cv.getVelocity(), maxDrift));
        }
    }

    public void updateCrash(int pieceIndex, int lane, double inPieceDistance) {
        if (trackPieces.get(pieceIndex).getRadius() == 0) {
            // the problem is in the previous curve
            pieceIndex--;
            while (trackPieces.get(pieceIndex).getRadius() == 0) {
                pieceIndex--;
            }
        }

        updateCrashPiece(pieceIndex, lane);
        pieceIndex--;

        if (pieceIndex < 0) {
            pieceIndex = trackPieces.size() + pieceIndex;
        }

        while ((trackPieces.get(pieceIndex).getRadius() == 0) ||
                trackPieces.get(pieceIndex).getAngleRemaining() == trackPieces.get(pieceIndex).getAngle()) {
            updateCrashPiece(pieceIndex, lane);
            pieceIndex--;
            if (pieceIndex < 0) {
                pieceIndex = trackPieces.size() + pieceIndex;
            }

        }
    }

    private void updateCrashPiece(int pieceIndex, int lane) {
        Map<Integer, CurveValue> ev;
        if (velocityEstimates.containsKey(pieceIndex)) {
            ev = velocityEstimates.get(pieceIndex);
        } else {
            ev = new HashMap<>();
            velocityEstimates.put(pieceIndex, ev);
        }
        CurveValue cv;
        if (ev.containsKey(lane)) {
            cv = ev.get(lane);
        } else {
            cv = new CurveValue();
            ev.put(lane, cv);
        }
        cv.setCrashed(true);
        cv.setVelocity(cv.getVelocity() * 0.8);

    }

    private double recomputeVelocity(int pieceIndex, double oldVelocity, double maxDrift) {
        if (trackHints.containsKey(pieceIndex)) {
            Hints h = trackHints.get(pieceIndex);

            if (h.isDriftTooHigh()) {
                return oldVelocity * 0.85;
            }
            h.setDriftTooHigh(false);
        }

        double exponent = (maxDrift / MAX_DRIFT * 2) - 1;
        double newFactor = 1 - (1.25 / (1 + Math.exp(-2 * exponent)));

        // Aggressive if the drift seen is low, conservative otherwise
        if (maxDrift / MAX_DRIFT < 0.25) {
            newFactor *= 2;
        } else if (maxDrift / MAX_DRIFT >= 0.75) {
            newFactor *= 0.8;
        }
        double newVelocity = oldVelocity + newFactor;
        return newVelocity;
    }

    private double adjustForCurves(int pieceIndex, double velocity) {
        if (trackHints.containsKey(pieceIndex)) {
            Hints h = trackHints.get(pieceIndex);
            if (h.isFollowedByCurve()) {
                velocity /= 2;
            } else if (h.isPrecededByCurve()) {
                velocity /= 2;
            } else if (h.driftTooHigh) {
                velocity *= 0.75;
            }
        }
        return velocity;
    }

    public double getVelocity(int pieceIndex, int lane) {
        Map<Integer, CurveValue> ev;
        if (velocityEstimates.containsKey(pieceIndex)) {
            ev = velocityEstimates.get(pieceIndex);
        } else {
            ev = new HashMap<>();
            velocityEstimates.put(pieceIndex, ev);
        }

        double v = 0;

        if (ev.containsKey(lane)) {
            v = adjustForCurves(pieceIndex, ev.get(lane).getVelocity());
        } else {
            CurveValue cv = new CurveValue();
            cv.setVelocity(getInitialVelocity(trackPieces.get(pieceIndex).getComputedCurvature(lane) * trackPieces.get(pieceIndex).getDirection()));
            ev.put(lane, cv);
            v = cv.getVelocity();
        }

        lastPieceVelocity.put(pieceIndex, v);
        return v;
    }

    private double getInitialVelocity(double curvature) {
        if (curvature >= 0.01) {
            return 3.0;
        }
        if (curvature <= 0.005) {
            return 7.0;
        }
        return 5.5;
    }

    public void updateDriftHint(int pieceIndex, int lane) {
        if (trackHints.containsKey(pieceIndex)) {
            Hints h = trackHints.get(pieceIndex);
            h.setDriftTooHigh(true);
        }
    }
}