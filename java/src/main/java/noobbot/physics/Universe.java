package noobbot.physics;

import org.apache.commons.math3.linear.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anita on 4/19/14.
 */
public class Universe {

    List<Double> predict(ForceCoeff forceCoeff,
                         List<Double> velocity) {

        List<Double> acceleration = new ArrayList<>(velocity.size());

        int degree = forceCoeff.getForces().size();

        RealVector forceVector = new ArrayRealVector(degree);
        for (int i = 0; i < degree; i++) {
            forceVector.setEntry(i, forceCoeff.getForces().get(i));
        }


        for (int i = 0; i < velocity.size(); i++) {
            RealVector velocityVector = new ArrayRealVector(degree);
            double pow = 1;
            double vel = velocity.get(i);
            for (int j = 0; j < degree; j++) {
                velocityVector.setEntry(j, pow);
                pow = pow * vel;
            }
            double accel = velocityVector.dotProduct(forceVector);
            acceleration.add(accel);
        }
        return acceleration;
    }

    List<ForceCoeff> solve(List<Double> velocity,
                           List<Double> acceleration,
                           int degree) {
        if (velocity.size() != acceleration.size()) {
            throw new IllegalArgumentException("Velocity size differs from acceleration");
        }
        if (degree > velocity.size()) {
            throw new IllegalArgumentException("Too small to compute");
        }

        List<ForceCoeff> output = new ArrayList<>();

        for (int i = 0; i + degree - 1 < velocity.size(); i++) {
            Array2DRowRealMatrix realMatrix = new Array2DRowRealMatrix(degree, degree);


            for (int j = 0; j < degree; j++) {
                double prod = 1;
                double vel = velocity.get(i + j);
                for (int k = 0; k < degree; k++) {
                    realMatrix.setEntry(j, k, prod);
                    prod = prod * vel;
                }
            }

            RealVector realVector = new ArrayRealVector(degree);
            for (int j = 0; j < degree; j++) {
                double accel = acceleration.get(i + j);
                realVector.setEntry(j, accel);
            }

            try {
                RealMatrix inverse = new LUDecomposition(realMatrix).getSolver().getInverse();
                RealVector forceField = inverse.operate(realVector);

                ArrayList<Double> out = new ArrayList<>(degree);
                for (int j = 0; j < forceField.getDimension(); j++) {
                    out.add(forceField.getEntry(j));
                }
                ForceCoeff forceCoeff = new ForceCoeff(i, out);
                output.add(forceCoeff);
            } catch (RuntimeException e) {
                continue;
            }
        }
        return output;
    }

    List<ForceCoeff> solveDrift(
            List<Double> driftList,
            List<Double> driftRateList,
            List<Double> driftAccelList,
            List<Double> velocityList) {

        List<ForceCoeff> output = new ArrayList<>();

        int degree = 3;

        for (int i = 0; i + degree - 1 < driftList.size(); i++) {
            Array2DRowRealMatrix realMatrix = new Array2DRowRealMatrix(degree, degree);

            for (int j = 0; j < degree; j++) {
                double v = velocityList.get(i + j);
                double drift = driftList.get(i + j);
                double driftRate = driftRateList.get(i + j);

                realMatrix.setEntry(j, 0, driftRate / v);
                realMatrix.setEntry(j, 1, drift / (v*v));
                realMatrix.setEntry(j, 2, drift);
            }

            RealVector realVector = new ArrayRealVector(degree);
            for (int j = 0; j < degree; j++) {
                double accel = driftAccelList.get(i + j);
                realVector.setEntry(j, accel);
            }

            try {
                RealMatrix inverse = new LUDecomposition(realMatrix).getSolver().getInverse();
                RealVector forceField = inverse.operate(realVector);

                ArrayList<Double> out = new ArrayList<>(degree);
                for (int j = 0; j < forceField.getDimension(); j++) {
                    out.add(forceField.getEntry(j));
                }
                ForceCoeff forceCoeff = new ForceCoeff(i, out);
                output.add(forceCoeff);
            } catch (RuntimeException e) {
                continue;
            }
        }
        return output;
    }
}
