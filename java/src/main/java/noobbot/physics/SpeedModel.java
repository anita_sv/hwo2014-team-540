package noobbot.physics;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import noobbot.math.LinearRegression;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import java.util.List;

/**
 */
@RequiredArgsConstructor @ToString
public class SpeedModel {

    private final ModelData modelData;

    /**
     */
    @Data
    public static class ModelData {

        private final double maxAcceleration;

        private final double coefficientOfViscosity;

        private final double mse;
    }

    /**
     */
    @Data
    public static class Input {

        private final double throttle;

        private final double velocity;
    }

    @Data
    public static class Output {
        private final double acceleration;
    }

    public static ModelData train(List<Input> input, List<Output> output) {
        if (input.size() != output.size()) {
            throw new IllegalArgumentException("input and output size must be equal");
        }
        int n = input.size();
        RealMatrix parameters = new Array2DRowRealMatrix(n, 2);
        for (int i = 0; i < n; i++) {
            parameters.setEntry(i, 0, input.get(i).getThrottle());
            parameters.setEntry(i, 1, -input.get(i).getVelocity());
        }

        RealVector outcome = new ArrayRealVector(n);
        for (int i = 0; i < n; i++) {
            outcome.setEntry(i, output.get(i).getAcceleration());
        }

        LinearRegression.RegressionResult result = LinearRegression.solve(parameters, outcome);

        RealVector solution = result.getSolution();
        System.out.printf("Speed Model Training: A=%f, Fs=%f (mse=%f)\n",
                solution.getEntry(0), solution.getEntry(1), result.getMeanSquaredError());
        return new ModelData(solution.getEntry(0), solution.getEntry(1), result.getMeanSquaredError());
    }


    public Output process(Input input) {
        RealVector parameters = new ArrayRealVector(2);
        parameters.setEntry(0, input.getThrottle());
        parameters.setEntry(1, -input.getVelocity());

        RealVector solution = new ArrayRealVector(2);
        solution.setEntry(0, modelData.getMaxAcceleration());
        solution.setEntry(1, modelData.getCoefficientOfViscosity());

        double acceleration = solution.dotProduct(parameters);

        return new Output(acceleration);
    }

    public double findAcceleration(Input currentState) {
        return currentState.getThrottle() * modelData.getMaxAcceleration() -
                currentState.getVelocity() * modelData.getCoefficientOfViscosity();
    }

    public double findVelocityAtConstThrottle(Input startState, double time) {
        double A = modelData.getMaxAcceleration();
        double Fs = modelData.getCoefficientOfViscosity();
        if (Fs != 0) {
            double limitVelocity = startState.getThrottle() * A / Fs;
            return limitVelocity - (limitVelocity - startState.getVelocity()) *
                    Math.exp(-Fs * time);
        } else {
            // v = u  + at
            return startState.getVelocity() + startState.getThrottle() * A * time;
        }
    }

    public double findDistanceAtConstThrottle(Input startState, double time) {
        double A = modelData.getMaxAcceleration();
        double Fs = modelData.getCoefficientOfViscosity();
        if (Fs != 0) {
            double limitVelocity = startState.getThrottle() * A / Fs;
            double term1 = limitVelocity * time;
            double term2 = (limitVelocity - startState.getVelocity()) * (Math.exp(-Fs * time) - 1) / Fs;
            return term1 + term2;
        } else {
            // S = ut + 1/2 at^2
            double u = startState.getVelocity();
            double a = startState.getThrottle() * A;
            return u * time + .5 * a * (time * time);
        }
    }

    public double maxSpeed() {
        double Fs = modelData.getCoefficientOfViscosity();
        if (Fs != 0) {
            return modelData.getMaxAcceleration() / Fs;
        } else {
            return Double.POSITIVE_INFINITY;
        }
    }

    public double findQuickThrottle(double acceleration, double entryVelocity) {
        return (entryVelocity * modelData.getCoefficientOfViscosity() + acceleration) /
                modelData.getMaxAcceleration();
    }

    public double findMaxEntryVelocity(double exitVelocity, double length) {
        return exitVelocity + length * modelData.coefficientOfViscosity;
    }

    public double findMinBrakingDistance(double entryVelocity, double exitVelocity) {
        return (entryVelocity - exitVelocity) / modelData.coefficientOfViscosity;
    }

    public double getError() {
        return  modelData.getMse();
    }

}
