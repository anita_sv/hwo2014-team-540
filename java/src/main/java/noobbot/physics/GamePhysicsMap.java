package noobbot.physics;

import lombok.Data;
import noobbot.data.DriftMetric;

/**
 * Created by anita on 4/29/14.
 */
@Data
public class GamePhysicsMap {

    private final SpeedModel speedModel;

    private final DriftModel driftModel;

    public GamePhysicsMap(SpeedModel speedModel, DriftModel driftModel) {
        this.speedModel = speedModel;
        this.driftModel = driftModel;
    }

    public double findVelocity(double entryVelocity, double throttle, double time) {
        return speedModel.findVelocityAtConstThrottle(new SpeedModel.Input(throttle, entryVelocity), time);
    }

    public double findTime(double entryVelocity, double throttle, double pieceLen, double dt) {
        return findTimeInternal(0, entryVelocity, throttle, pieceLen, dt);
    }
    public double findTimeInternal(double startTime,
                                   double entryVelocity, double throttle, double pieceLen,
                                   double dt) {
        if (throttle <= 0) {
            return Double.MAX_VALUE;
        }
        for (double t = startTime; ; t += dt) {
            double distance = speedModel.findDistanceAtConstThrottle(
                    new SpeedModel.Input(throttle, entryVelocity), t);
            if (distance > pieceLen) {
                return t;
            }
        }
    }

    public double findMaxDrift(double entryVelocity, double throttle,
                               double t, DriftMetric driftMetrics,
                               double signedCurvature, double dt) {
        double drift = driftMetrics.getDriftDegrees();
        double driftRate = driftMetrics.getDriftRateDegrees();

        double maxDrift = drift;

        for (double tt = 0; tt < t ; tt += dt) {
            double velocity = findVelocity(entryVelocity, throttle, tt);

            double acceleration = speedModel.findAcceleration(new SpeedModel.Input(throttle, velocity));

            DriftModel.Input input = DriftModel.Input.builder()
                    .setAcceleration(acceleration)
                    .setCurvature(signedCurvature)
                    .setDriftDegrees(drift)
                    .setDriftDegreesRate(driftRate)
                    .setVelocity(velocity)
                    .build();

            double driftAccel = driftModel.process(input).getDriftDegreesAccel();

            double ldt = ((tt + dt) < t ) ? dt : (t-tt);
            double nextDriftRate = driftRate + driftAccel * ldt;

            drift = drift + driftRate * ldt;
            driftRate = nextDriftRate;

            if (Math.abs(maxDrift) < Math.abs(drift)) {
                maxDrift = drift;
            }
        }

        return maxDrift;

    }

    public DriftMetric findExitDrift(double entryVelocity, double throttle,
                                     double t, DriftMetric driftMetrics,
                                     double signedCurvature,
                                     double dt) {
        double drift = driftMetrics.getDriftDegrees();
        double driftRate = driftMetrics.getDriftRateDegrees();
        double driftAccel = driftMetrics.getDriftAccelDegrees();

        for (double tt = 0; tt < t ; tt += dt) {
            double velocity = findVelocity(entryVelocity, throttle, tt);

            double acceleration = speedModel.findAcceleration(new SpeedModel.Input(throttle, velocity));

            DriftModel.Input input = DriftModel.Input.builder()
                    .setAcceleration(acceleration)
                    .setCurvature(signedCurvature)
                    .setDriftDegrees(drift)
                    .setDriftDegreesRate(driftRate)
                    .setVelocity(velocity)
                    .build();

            driftAccel = driftModel.process(input).getDriftDegreesAccel();

            double ldt = tt + dt < t ? dt : (t-tt);
            double nextDriftRate = driftRate + driftAccel * ldt;
            drift = drift + driftRate * ldt;
            driftRate = nextDriftRate;
        }

        return new DriftMetric(drift, driftRate, driftAccel);
    }

    public double maxSpeed() {
        return speedModel.maxSpeed();
    }

    public double findQuickThrottle(double acceleration, double entryVelocity) {
        return speedModel.findQuickThrottle(acceleration, entryVelocity);
    }

    public double findAcceleration(double throttle, double entryVelocity) {
        return speedModel.findAcceleration(new SpeedModel.Input(throttle, entryVelocity));
    }

    public double findDriftAcceleration(DriftModel.Input input) {
        return driftModel.process(input).getDriftDegreesAccel();
    }

    /**
     */
    public double findMaxCurveVelocity(double curvature,
                                       double length,
                                       double entryDrift,
                                       double entryDriftRate,
                                       double maxDrift) {
        DriftMetric entryDriftMetric = new DriftMetric(entryDrift, entryDriftRate, 0);
        // TODO(anita): Speed this up by search
        for (int vel = 1; vel <= 100; vel++) {
            double velocity = vel * .01 * speedModel.maxSpeed();
            double throttle = speedModel.findQuickThrottle(0, velocity);
            double time = length / velocity;
            double drift = findMaxDrift(velocity, throttle, time, entryDriftMetric, curvature, 0.5);
            if (drift > maxDrift) {
                return (vel - 1) * .01 * speedModel.maxSpeed();
            }
        }
        return speedModel.maxSpeed();
    }

    public double findMaxThrottle(double curvature, double length, double entryDrift,
                                  double entryDriftRate, double maxDrift) {
        double maxVelocity = findMaxCurveVelocity(curvature, length, entryDrift, entryDriftRate, maxDrift);
        return speedModel.findQuickThrottle(0, maxVelocity);
    }

    public double findMaxCurveDrift(double velocity, double curvature, double length) {
        DriftMetric entryDriftMetric = new DriftMetric(0, 0, 0);
        double time = length / velocity;
        return findMaxDrift(velocity, 0.0, time, entryDriftMetric, curvature, 1.0);
    }

    public double findMaxEntryVelocity(double exitVelocity, double length) {
        return speedModel.findMaxEntryVelocity(exitVelocity, length);
    }

    public double findDistance(double entryVelocity, double throttle, double timeEstimate) {
        return speedModel.findDistanceAtConstThrottle(
                new SpeedModel.Input(throttle, entryVelocity), timeEstimate);
    }

    public double findTimeNew(double entryVelocity, double throttle, double laneLeft) {
        double startTime = 0;
        for (double dt = 1; dt > 0.01; dt = dt / 2) {
            startTime = findTimeInternal(startTime, entryVelocity, throttle, laneLeft, dt) - dt;
        }
        return startTime;
    }

    /**
     * @param entryVelocity
     * @param exitVelocity
     * @return if exitVelocity < entryVelocity returns minimum braking distance, otherwise
     *     returns a negative number.
     */
    public double findMinBrakingDistance(double entryVelocity, double exitVelocity) {
        return speedModel.findMinBrakingDistance(entryVelocity, exitVelocity);
    }
}
