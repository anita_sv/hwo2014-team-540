package noobbot.physics;

import lombok.Data;

import java.util.List;

/**
 * Created by anita on 4/19/14.
 */
@Data
public class ForceCoeff {

    private final int time;

    private final List<Double> forces;
}
