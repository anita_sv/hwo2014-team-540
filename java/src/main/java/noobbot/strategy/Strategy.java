package noobbot.strategy;

import com.google.inject.ImplementedBy;
import noobbot.data.CarControl;

/**
 * Created by anita on 4/25/14.
 */
@ImplementedBy(SimpleStrategy.class)
public interface Strategy {

    public CarControl takeAction(CarControl adaptiveControl);
}
