package noobbot.strategy;

import com.google.inject.Inject;
import noobbot.controller.PhysicsStore;
import noobbot.data.*;
import noobbot.msg.GameInit;
import noobbot.msg.TrackPiece;
import noobbot.physics.GamePhysics;

import java.util.List;

/**
 * Created by anita on 4/25/14.
 */

public class SimpleStrategy
        implements Strategy {

    private final Cars allCarDetails;
    private final List<TrackPiece> trackPieces;
    private final GameInit gameData;
    private final PhysicsStore physics;
    private RealtimeCarData myCar;
    //LearningHistory model;


    private int nextSwitchPiece = 1;

    @Inject
    public SimpleStrategy(Cars allCarDetails, GameInit gameData, PhysicsStore physics) {
        this.allCarDetails = allCarDetails;
        this.gameData = gameData;
        this.trackPieces = gameData.getRace().getTrack().getPieces();
        this.physics = physics;
        //model = new LearningHistory(gameData.getRace().getRaceSession().getLaps() , trackPieces.size(), gameData.getRace().getTrack().getLanes().size());
        //model.doFirstPlan(physics.get(), trackPieces);
        System.out.println("Strat - Lap, Piece, InPieceDistance, Velocity, Drift,  DriftRate");
    }

    @Override
    public CarControl takeAction(CarControl adaptiveControl) {
         myCar = allCarDetails.myCar();

        System.out.printf("Strat - %d, %d, %.3f, %.3f, %.3f, %.3f%n",
                myCar.getLap(), myCar.getPieceIndex(), myCar.getInPieceDistance(), myCar.getVelocity(),
                myCar.getDrift(), myCar.getDriftRate());

        if (myCar.getPieceIndex() + 1 == nextSwitchPiece) {
            SwitchLane potentialSwitch = checkForSwitch(myCar.getLane());
            if (potentialSwitch == SwitchLane.LEFT) {
                if (allCarDetails.getLaneCongestion(myCar.getLane() - 1, 2) < 1){
                    return CarControl.switchLeft();
                } else {
                    System.out.println("Too much congestion.. Deferring switch");
                }
            }
            if (potentialSwitch == SwitchLane.RIGHT) {
                if (allCarDetails.getLaneCongestion(myCar.getLane() + 1, 2) < 1){
                    return CarControl.switchRight();
                } else {
                    System.out.println("Too much congestion.. Deferring switch");
                }
            }
        }
        if (adaptiveControl.getAction() == Action.THROTTLE) {
            if (allCarDetails.isCarInFrontSpeeding()) {
                System.out.println("Strategy - Let the car ahead brake");
                return CarControl.applyThrottle(1.0);
            }

            if (allCarDetails.isCarBlocked()) {
                System.out.println("Strategy - Let us push them out");
                if ((trackPieces.get(myCar.getPieceIndex()).getStraightLength() > 300) &&
                        (myCar.getTurboAvailable() > 0) && (!myCar.isInTurbo()))  {
                    return CarControl.applyTurbo();
                }
            }

            if ((trackPieces.get(myCar.getPieceIndex()).getStraightLength() >= 400) &&
                    (myCar.getTurboAvailable() > 0) && (!myCar.isInTurbo()))  {
                System.out.println("Strategy - Straight piece - let us party");
                return CarControl.applyTurbo();
            }

            if ((gameData.getRace().getTrack().getPieces().get(myCar.getPieceIndex()).isPotentialTurbo()) &&
                    (myCar.getTurboAvailable() > 0) &&
                    (!myCar.isInTurbo())) {
                return CarControl.applyTurbo();
            }
        }

        if (adaptiveControl.getAction() == Action.APPLY_TURBO) {
            return CarControl.ping();
        }

        return adaptiveControl;
    }


    private SwitchLane checkForSwitch(int currentLane) {
        int currentTrackIndex = nextSwitchPiece;

        boolean firstSwitchFound = false;
        boolean secondSwitchFound = false;



        int totalLanes = gameData.getRace().getTrack().getLanes().size();
        // Find next two switch pieces
        while (!firstSwitchFound) {
            TrackPiece nextPiece = trackPieces.get(currentTrackIndex);
            if (nextPiece.isSwitchLane()) {
                firstSwitchFound = true;
            }
            currentTrackIndex  = (currentTrackIndex+1)%trackPieces.size();
        }


        double[] trackLengths = new double[3];
        boolean[] tooSharp = new boolean[]{false, false, false};
        // Continue on counting track lengths till another switch piece
        while (!secondSwitchFound) {
            if (trackPieces.get(currentTrackIndex).isSwitchLane()) {
                secondSwitchFound = true;
                nextSwitchPiece = currentTrackIndex;
            } else {
                trackLengths[1] += trackPieces.get(currentTrackIndex).getComputedLaneLength(currentLane);
                if (trackPieces.get(currentTrackIndex).getComputedLaneRadius(currentLane) < 60) {
                    tooSharp[1] = true;
                }
                if (currentLane > 0) {
                    if (trackPieces.get(currentTrackIndex).getComputedLaneRadius(currentLane - 1) < 60) {
                        tooSharp[0] = true;
                    }
                    trackLengths[0] += trackPieces.get(currentTrackIndex).getComputedLaneLength(currentLane - 1);
                } else {
                    trackLengths[0] = trackLengths[1];
                }
                if (currentLane < totalLanes - 1) {
                    if (trackPieces.get(currentTrackIndex).getComputedLaneRadius(currentLane + 1) < 60) {
                        tooSharp[2] = true;
                    }
                    trackLengths[2] += trackPieces.get(currentTrackIndex).getComputedLaneLength(currentLane + 1);
                } else {
                    trackLengths[2] = trackLengths[1];
                }
            }
            currentTrackIndex++;
            if (currentTrackIndex == trackPieces.size() - 1)
                currentTrackIndex = 0;
        }

        System.out.printf("Current Piece: %d, Track Left: %.3f, Center : %.3f, Right: %.3f, Next Switch Piece : %d%n", myCar.getPieceIndex(), trackLengths[0], trackLengths[1], trackLengths[2], nextSwitchPiece);
        if ((trackLengths[0] < trackLengths[1])) {
            System.out.printf("Switching from lane %d to %d%n", currentLane, currentLane-1);
            return SwitchLane.LEFT;
        } else if ((trackLengths[2] < trackLengths[1])) {
            System.out.printf("Switching from lane %d to %d%n", currentLane, currentLane+1);
            return SwitchLane.RIGHT;
        }
        return SwitchLane.STAY;

//        if ((trackLengths[0] <= trackLengths[1]) && (!tooSharp[0])) {
//            System.out.printf("Switching from lane %d to %d%n", currentLane, currentLane-1);
//            return SwitchLane.LEFT;
//        } else if ((trackLengths[2] <= trackLengths[1]) && (!tooSharp[2])) {
//            System.out.printf("Switching from lane %d to %d%n", currentLane, currentLane+1);
//            return SwitchLane.RIGHT;
//        } else if (tooSharp[0] && tooSharp[1] && tooSharp[2]){
//            // Switch to the longest track since all curves are too sharp
//            if (trackLengths[0] > trackLengths[1]) {
//                return SwitchLane.LEFT;
//            } else {
//                return SwitchLane.RIGHT;
//            }
//        }
//
//        return SwitchLane.STAY;


    }

}
