package noobbot.adapters;

import com.google.gson.*;
import lombok.RequiredArgsConstructor;
import noobbot.base.MessageType;
import noobbot.base.MsgWrapper;

import javax.inject.Provider;
import java.lang.reflect.Type;

/**
 * Created by anita on 4/17/14.
 */
@RequiredArgsConstructor
public class MsgWrapperDeserializer implements JsonDeserializer<MsgWrapper> {

    private final Provider<Gson> gsonProvider;

    @Override
    public MsgWrapper deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {

        JsonObject me = json.getAsJsonObject();

        String msgType = me.get("msgType").getAsString();
        JsonElement data = me.get("data");

        MessageType messageType = MessageType.find(msgType);
        Object value;
        if (messageType != messageType.UNKNOWN) {
            if (messageType.getClazz() != null) {
                value = gsonProvider.get().fromJson(data, messageType.getClazz());
            } else {
                value = null;
            }
        } else {
            value = data;
        }

        JsonElement gameIdElem = me.get("gameId");
        JsonElement gameTickElem = me.get("gameTick");

        String gameId = gameIdElem != null ? gameIdElem.getAsString() : null;
        Integer gameTick = gameTickElem != null ? gameTickElem.getAsInt() : null;

        return new MsgWrapper(messageType, value, gameId, gameTick);
    }
}
