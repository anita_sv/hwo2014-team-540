package noobbot.adapters;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import lombok.RequiredArgsConstructor;
import noobbot.msg.CarPositions;

import javax.inject.Provider;
import java.lang.reflect.Type;

/**
 * Created by anita on 4/17/14.
 */
@RequiredArgsConstructor
public class CarPositionsSerializer implements JsonSerializer<CarPositions> {

    private final Provider<Gson> gsonProvider;

    @Override
    public JsonElement serialize(CarPositions src, Type typeOfSrc, JsonSerializationContext context) {
        return gsonProvider.get().toJsonTree(src.getCarPositionList());
    }
}
