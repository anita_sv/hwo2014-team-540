package noobbot.adapters;

import com.google.gson.*;
import lombok.RequiredArgsConstructor;
import noobbot.base.MsgWrapper;

import javax.inject.Provider;
import java.lang.reflect.Type;

/**
 * Created by anita on 4/18/14.
 */
@RequiredArgsConstructor
public class MsgWrapperSerializer implements JsonSerializer<MsgWrapper> {

    private final Provider<Gson> gsonProvider;

    @Override
    public JsonElement serialize(MsgWrapper src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("msgType", src.getMsgType().getMsgType());

        jsonObject.add("data", gsonProvider.get().toJsonTree(src.getData()));
        jsonObject.addProperty("gameId", src.getGameId());
        jsonObject.addProperty("gameTick", src.getGameTick());

        return jsonObject;
    }
}
