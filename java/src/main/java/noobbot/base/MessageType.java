package noobbot.base;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import noobbot.msg.*;

/**
 * Created by anita on 4/17/14.
 */
@RequiredArgsConstructor @Getter
public enum MessageType {
    CREATE_RACE("createRace", CreateRace.class),
    JOIN_RACE("joinRace", CreateRace.class),
    JOIN("join", Join.class),
    YOUR_CAR("yourCar", CarIdentity.class),
    GAME_INIT("gameInit", GameInit.class),
    GAME_START("gameStart", null),
    CAR_POSITIONS("carPositions", CarPositions.class),
    THROTTLE("throttle", Double.class),
    GAME_END("gameEnd", GameEnd.class),
    PING("ping", null),
    CRASH("crash", CarIdentity.class),
    SPAWN("spawn", CarIdentity.class),
    LAP_FINISHED("lapFinished", LapFinished.class),
    FINISH("finish", CarIdentity.class),
    TOURNAMENT_END("tournamentEnd", null),
    DNF("dnf", CarIdentity.class),
    ERROR("error", String.class),
    SWITCH_LANE("switchLane", String.class),
    TURBO_AVAILABLE("turboAvailable", TurboAvailable.class),
    TURBO("turbo", String.class),
    TURBO_START("turboStart", CarIdentity.class),
    TURBO_END("turboEnd", CarIdentity.class),
    UNKNOWN("Unknown", null)

    ;
    private final String msgType;

    private final Class<?> clazz;

    public static MessageType find(String msgType) {
        for (MessageType messageType : values()) {
            if (messageType.getMsgType().equals(msgType)) {
                return messageType;
            }
        }
        return UNKNOWN;
    }
}
