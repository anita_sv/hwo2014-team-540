package noobbot.base;

import lombok.Data;

/**
 */
@Data
public class MsgWrapper {

    private final MessageType msgType;

    private final Object data;

    private final String gameId;

    private final Integer gameTick;
}
